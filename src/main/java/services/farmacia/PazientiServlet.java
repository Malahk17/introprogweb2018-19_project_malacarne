package services.farmacia;

import beans.Paziente;
import dao.DAOPazienti;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "PazientiServlet")
public class PazientiServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOPazienti daoPazienti = new DAOPazienti();
        ArrayList<Paziente> pazienti = daoPazienti.getAllPazienti();
        StringBuilder suggestion = new StringBuilder("[");

        boolean first = true;
        for (Paziente p : pazienti) {
            if (!first) {
                suggestion.append(',');
            } else {
                first = false;
            }
            suggestion.append('"').append(p.getCognome()).append(' ').append(p.getNome())
                    .append(' ').append(p.getCodiceFiscale()).append('"');
        }
        suggestion.append(']');

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println(suggestion.toString());
    }
}
