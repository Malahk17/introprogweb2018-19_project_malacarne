package services.farmacia;

import beans.Medicinale;
import beans.Paziente;
import beans.Ricetta;
import dao.DAOMedicinali;
import dao.DAOPazienti;
import dao.DAORicette;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "RicettePazienteServlet")
public class RicettePazienteServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String pazienteString = request.getParameter("paziente");
        DAOMedicinali daoMedicinali = new DAOMedicinali();
        DAOPazienti daoPazienti = new DAOPazienti();
        DAORicette daoRicette = new DAORicette();

        Paziente paziente = daoPazienti.getPazienteByCodiceFiscale(pazienteString.substring(pazienteString.lastIndexOf(' ')+1));
        ArrayList<Ricetta> ricette = daoRicette.getRicetteByIDPaziente(paziente.getID(), false);
        StringBuilder suggestion = new StringBuilder("[");

        boolean first = true;
        Medicinale m;
        for (Ricetta r : ricette) {
            m = daoMedicinali.getMedicinaleByID(r.getMedicinale());
            if (!first) {
                suggestion.append(",");
            } else {
                first = false;
            }
            suggestion.append("{\"id\":").append(String.valueOf(r.getID())).append(",\"medicinale\":\"")
                    .append(m.getNome()).append("\"}");
        }
        suggestion.append("]");

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println(suggestion.toString());
        out.flush();
        out.close();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
    }
}
