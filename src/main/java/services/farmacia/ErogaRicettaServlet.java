package services.farmacia;

import beans.Ricetta;
import dao.DAOFarmacie;
import dao.DAOMedicinali;
import dao.DAORicette;
import dao.DAOTickets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ErogaRicettaServlet")
public class ErogaRicettaServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOFarmacie daoFarmacie = new DAOFarmacie();
        DAOMedicinali daoMedicinali = new DAOMedicinali();
        DAORicette daoRicette = new DAORicette();
        DAOTickets daoTickets = new DAOTickets();

        int idRicetta = Integer.parseInt(request.getParameter("id-ricetta"));
        daoRicette.setRicettaRitirata(idRicetta, Integer.parseInt(request.getSession().getAttribute("ID").toString()));
        Ricetta r = daoRicette.getRicettaByID(idRicetta);

        int idPaziente = r.getPaziente();
        String causale = "Ricetta '" + daoMedicinali.getMedicinaleByID(r.getMedicinale()).getNome()
                + "' presso " + daoFarmacie.getFarmaciaByID(r.getFarmacia()).getNome();
        double importo = 3.0;
        daoTickets.addTicket(idPaziente, causale, importo);

        response.sendRedirect(request.getContextPath() + "/farmacia-home.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
