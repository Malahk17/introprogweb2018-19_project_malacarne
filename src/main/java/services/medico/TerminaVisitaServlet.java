package services.medico;

import beans.Medicinale;
import beans.Medico;
import beans.Paziente;
import dao.*;
import org.apache.commons.codec.language.bm.Languages;
import services.general.MailDispatcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;

@WebServlet(name = "TerminaVisitaServlet")
public class TerminaVisitaServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOListaEsami daoLE = new DAOListaEsami();
        DAOEsami daoEsami = new DAOEsami();
        DAOMedici daoMedici = new DAOMedici();
        DAOMedicinali daoMedicinali = new DAOMedicinali();
        DAOPazienti daoPazienti = new DAOPazienti();
        DAORicette daoRicette = new DAORicette();
        DAOUtenti daoUtenti = new DAOUtenti();

        int idMedico = Integer.parseInt(request.getSession().getAttribute("ID").toString());
        int idPaziente = Integer.parseInt(request.getParameter("paziente"));

        Paziente p = daoPazienti.getPazienteByID(idPaziente);
        MailDispatcher mail = new MailDispatcher();
        String to = daoUtenti.getMailPaziente(idPaziente);
        String subject;
        String text;

        int id;
        boolean enoughArgs = false;
        String medicinali = request.getParameter("medicinali");
        if (medicinali != null && !medicinali.isBlank()) {
            String[] splits = medicinali.split(",");

            for (String s : splits) {
                id = daoMedicinali.getMedicinaleByNome(s).getID();
                daoRicette.addRicetta(idPaziente, id, idMedico);

                subject = "Nuova ricetta disponibile per te!";
                text = "Gentile " + p.getNome() + ",\nÉ stato inserita una nuova ricetta ('" + s +
                        "') sul Sistema Sanitario online.\nLa preghiamo di controllare.";
                mail.sendMail(to, subject, text);
            }

            enoughArgs = true;
            daoMedici.updateUltimaRicetta(idMedico);
        }

        String esami = request.getParameter("esami");
        if (esami != null && !esami.isBlank()) {
            String[] splits = esami.split(",");

            for (String s : splits) {
                id = daoLE.getIdEsamePrescrivibileByNome(s);
                daoEsami.addEsame(idPaziente, id);

                subject = "Nuovo esame prescritto per te!";
                text = "Gentile " + p.getNome() + ",\nLe é stato prescritto un esame ('" + s +
                        "') sul Sistema Sanitario online.\nLa preghiamo di controllare.";
                mail.sendMail(to, subject, text);
            }

            enoughArgs = true;
        }

        Medico self = daoMedici.getMedicoByID(idMedico);
        if (self.isBase()) {
            if (enoughArgs) {
                daoMedici.updateUltimaVisita(idMedico);
                response.setStatus(HttpServletResponse.SC_OK);
            } else {
                response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE);
            }
        } else {
            DAOTickets daoTickets = new DAOTickets();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String causale = "Visita specialistica presso '" + self.getCognome() + " " + self.getNome() +
                    "' in data " + sdf.format(new Date(System.currentTimeMillis())) + ".";
            double importo = 50.0;
            daoTickets.addTicket(idPaziente, causale, importo);

            daoMedici.updateUltimaVisita(idMedico);
            response.setStatus(HttpServletResponse.SC_OK);
        }
    }
}
