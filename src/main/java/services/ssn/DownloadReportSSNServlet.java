package services.ssn;

import beans.*;
import dao.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Date;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@WebServlet(name = "DownloadReportSSNServlet")
public class DownloadReportSSNServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        downloadXLS(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        downloadXLS(request, response);
    }

    private void downloadXLS(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String provincia = request.getParameter("select-provincia");
        String selectMedico = request.getParameter("select-medico");
        String xlsReport;

        DAOFarmacie daoFarmacie = new DAOFarmacie();
        DAOMedici daoMedici = new DAOMedici();
        DAOMedicinali daoMedicinali = new DAOMedicinali();
        DAOPazienti daoPazienti = new DAOPazienti();
        DAORicette daoRicette = new DAORicette();
        ArrayList<Ricetta> ricette = new ArrayList<>();

        if (selectMedico.isBlank()) {
            // Report per provincia
            ArrayList<Medico> medici = daoMedici.getMediciByProvincia(provincia);

            for (Medico m : medici) {
                ricette.addAll(daoRicette.getRicetteByIDMedico(m.getID(), true));
            }

            xlsReport = "report-" + provincia + ".xls";
        } else {
            // Report per medico
            Medico medico = daoMedici.getMedicoByCognomeNome(selectMedico);

            ricette.addAll(daoRicette.getRicetteByIDMedico(medico.getID(), true));

            xlsReport = "report-" + selectMedico.replace(" ", "") + ".xls";
        }

        Workbook wb = new HSSFWorkbook();
        CreationHelper helper = wb.getCreationHelper();
        Sheet sheet = wb.createSheet();

        CellStyle cellStyle = wb.createCellStyle();
        cellStyle.setAlignment(HorizontalAlignment.CENTER);
        cellStyle.setVerticalAlignment(VerticalAlignment.CENTER);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Row row = sheet.createRow(0);
        createCell(wb, row, 0, "Ora:", cellStyle);
        createCell(wb, row, 1, "Farmaco:", cellStyle);
        createCell(wb, row, 2, "Farmacia:", cellStyle);
        createCell(wb, row, 3, "Medico Prescrivente:", cellStyle);
        createCell(wb, row, 4, "Paziente:", cellStyle);

        int rn = 1;
        Time t;
        Farmacia farmacia;
        Medico medico;
        Medicinale medicinale;
        Paziente paziente;
        for (Ricetta r : ricette) {
            row = sheet.createRow(rn);

            t = new Time(r.getDataPrescrizione().getTime());
            medicinale = daoMedicinali.getMedicinaleByID(r.getMedicinale());
            farmacia = daoFarmacie.getFarmaciaByID(r.getFarmacia());
            medico = daoMedici.getMedicoByID(r.getMedico());
            paziente = daoPazienti.getPazienteByID(r.getPaziente());

            createCell(wb, row, 0, t.toString(), cellStyle);
            createCell(wb, row, 1, medicinale.getNome(), cellStyle);
            createCell(wb, row, 2, farmacia.getNome(), cellStyle);
            createCell(wb, row, 3, medico.getCognome() + " " + medico.getNome(), cellStyle);
            createCell(wb, row, 4, paziente.getCognome() + " " + paziente.getNome(), cellStyle);

            rn++;
        }

        sheet.autoSizeColumn(0);
        sheet.autoSizeColumn(1);
        sheet.autoSizeColumn(2);
        sheet.autoSizeColumn(3);
        sheet.autoSizeColumn(4);

        ServletOutputStream out = response.getOutputStream();
        response.setContentType("application/vnd.openxml");
        response.setHeader("Content-Disposition", "attachment; filename=" + xlsReport);

        wb.write(out);
        out.flush();
        out.close();
    }

    private void createCell(Workbook wb, Row row, int col, String value, CellStyle cellStyle) {
        Cell cell = row.createCell(col);
        cell.setCellValue(value);
        cell.setCellStyle(cellStyle);
    }

}
