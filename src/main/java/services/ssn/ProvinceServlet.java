package services.ssn;

import beans.Provincia;
import dao.DAOProvince;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "ProvinceServlet")
public class ProvinceServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOProvince daoProvince = new DAOProvince();
        ArrayList<Provincia> province = daoProvince.getAllProvince();
        StringBuilder provinceString = new StringBuilder("[");
        boolean first = true;

        for (Provincia p : province) {
            if (first) {
                first = false;
            } else {
                provinceString.append(',');
            }
            provinceString.append('"').append(p.getNome()).append('"');
        }
        provinceString.append("]");

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println(provinceString.toString());
    }
}
