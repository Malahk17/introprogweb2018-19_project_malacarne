package services.ssn;

import beans.Medico;
import beans.Provincia;
import dao.DAOMedici;
import dao.DAOProvince;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "MediciProvinciaServlet")
public class MediciProvinciaServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOMedici daoMedici = new DAOMedici();
        DAOProvince daoProvince = new DAOProvince();
        String provincia = request.getParameter("provincia");
        ArrayList<Medico> medici = daoMedici.getMediciByProvincia(daoProvince.getProvinciaByNome(provincia).getSigla(), true);
        StringBuilder mediciString = new StringBuilder("[");
        boolean first = true;

        for (Medico m : medici) {
            if (first) {
                first = false;
            } else {
                mediciString.append(',');
            }
            mediciString.append('"').append(m.getCognome()).append(' ').append(m.getNome()).append('"');
        }
        mediciString.append("]");

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println(mediciString.toString());
    }

}
