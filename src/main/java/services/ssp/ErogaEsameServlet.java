package services.ssp;

import beans.Esame;
import beans.EsamePrescrivibile;
import dao.DAOEsami;
import dao.DAOListaEsami;
import dao.DAOTickets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@WebServlet(name = "ErogaEsameServlet")
public class ErogaEsameServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOEsami daoEsami = new DAOEsami();
        DAOTickets daoTickets = new DAOTickets();
        DAOListaEsami daoLE = new DAOListaEsami();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        int idEsame = Integer.parseInt(request.getParameter("id"));
        String esito = request.getParameter("esito");

        String ruolo = session.getAttribute("ruolo").toString();
        if (ruolo.equals("M")) {
            daoEsami.erogaEsame(idEsame, esito, Integer.parseInt(session.getAttribute("ID").toString()));
        } else {
            daoEsami.erogaEsame(idEsame, esito, -1);
        }

        Esame e = daoEsami.getEsameByID(idEsame);

        if (!e.isRichiamo() || !ruolo.equals("M")) {
            int idPaziente = e.getPaziente();
            String causale = "Esame '" + daoLE.getEsamePrescrivibiliByID(e.getEsamePrescrivibile()).getNome()
                    + "' in data " + sdf.format(e.getData());
            double importo = 11.0;
            daoTickets.addTicket(idPaziente, causale, importo);
        }

        response.setStatus(HttpServletResponse.SC_OK);
    }

}
