package services.ssp;

import beans.Paziente;
import dao.DAOEsami;
import dao.DAOPazienti;
import dao.DAOUtenti;
import services.general.MailDispatcher;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet(name = "EmettiRichiamoServlet")
public class EmettiRichiamoServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAOEsami daoRichiami = new DAOEsami();
        DAOUtenti daoUtenti = new DAOUtenti();

        String nomeEsame = request.getParameter("richiamo-suggestion");
        String provincia = request.getSession().getAttribute("ID").toString();
        String minString = request.getParameter("min");
        String maxString = request.getParameter("max");
        int min = minString.isBlank() ? -1 : Integer.parseInt(minString);
        int max = maxString.isBlank() ? -1 : Integer.parseInt(maxString);

        ArrayList<Paziente> pazienti;
        if (min >= 0 && max <= 150 && min <= max) {
            pazienti = daoRichiami.addRichiamoMinMax(nomeEsame, provincia, min, max);
        } else if(min == -1 && max >= 0 && max <=150) {
            pazienti = daoRichiami.addRichiamoMax(nomeEsame, provincia, max);
        } else if (max == -1 && min >= 0 && min <= 150) {
            pazienti = daoRichiami.addRichiamoMin(nomeEsame, provincia, min);
        } else if (min == -1 && max == -1) {
            pazienti = daoRichiami.addRichiamo(nomeEsame, provincia);
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        MailDispatcher mail = new MailDispatcher();
        String to;
        String subject = "Nuovo richiamo prescritto per te!";
        String text = "Gentile ";
        for (Paziente p : pazienti) {
            to = daoUtenti.getMailPaziente(p.getID());
            text += p.getNome() + ",\nLe é stato prescritto un richiamo sul Sistema Sanitario online.\n" +
                    "La preghiamo di controllare.";
            mail.sendMail(to, subject, text);
        }

        response.sendRedirect(request.getContextPath() + "/ssp-home.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
    }

}
