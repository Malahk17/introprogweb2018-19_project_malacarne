package services.paziente;

import beans.Medico;
import beans.Paziente;
import dao.DAOMedici;
import dao.DAOPazienti;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet(name = "CambioMedicoServlet")
public class CambioMedicoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOPazienti daoPazienti = new DAOPazienti();
        DAOMedici daoMedici = new DAOMedici();

        Medico m = daoMedici.getMedicoByCognomeNome(request.getParameter("nuovo-medico"));
        daoPazienti.updateMedico(Integer.parseInt((String) session.getAttribute("ID")), m.getID());

        response.sendRedirect(request.getContextPath() + "/cittadino-home.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOPazienti daoPazienti = new DAOPazienti();
        DAOMedici daoMedici = new DAOMedici();
        StringBuilder medici = new StringBuilder("[");
        ArrayList<Medico> mediciInProvincia = new ArrayList<>();

        int idPaziente = Integer.parseInt((String) session.getAttribute("ID"));
        Paziente paziente = daoPazienti.getPazienteByID(idPaziente);

        boolean first = true;
        mediciInProvincia = daoMedici.getMediciByProvincia(paziente.getProvincia(), true);
        for (Medico m : mediciInProvincia) {
            if (m.getID() != paziente.getMedicoBase()) {
                if (!first) {
                    medici.append(",");
                } else {
                    first = false;
                }
                medici.append("\"").append(m.getCognome()).append(" ").append(m.getNome()).append("\"");
            }
        }
        medici.append("]");

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();
        out.println(medici.toString());
        out.flush();
        out.close();
    }
}
