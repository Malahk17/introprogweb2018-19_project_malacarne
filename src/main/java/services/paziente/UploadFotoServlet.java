package services.paziente;

import beans.Foto;
import dao.DAOFoto;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.Date;
import java.time.LocalDate;

@WebServlet(name = "UploadFotoServlet")
@MultipartConfig
public class UploadFotoServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOFoto daoFoto = new DAOFoto();

        int id = Integer.parseInt(session.getAttribute("ID").toString());
        Date data = new Date(System.currentTimeMillis());
        byte[] img;

        Foto fotoAttiva = daoFoto.getFotoByIDPazienteAttiva(id);
        if (fotoAttiva != null) {
            LocalDate oldData = fotoAttiva.getData().toLocalDate();
            LocalDate newData = data.toLocalDate();

            if (oldData.getYear() != newData.getYear() || oldData.getMonthValue() != newData.getMonthValue()
                    || oldData.getDayOfMonth() != newData.getDayOfMonth()) {
                Part imgPart = request.getPart("upload-immagine");

                if (imgPart != null) {
                    img = imgPart.getInputStream().readAllBytes();
                    daoFoto.insertFoto(id, data, img);
                }
            }
        } else {
            Part imgPart = request.getPart("upload-immagine");

            if (imgPart != null) {
                img = imgPart.getInputStream().readAllBytes();
                daoFoto.insertFoto(id, data, img);
            }
        }

        response.sendRedirect(request.getContextPath() + "/cittadino-home.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
    }
}
