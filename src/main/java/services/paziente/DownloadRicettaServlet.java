package services.paziente;

import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import dao.DAORicette;
import dao.DAOPazienti;
import dao.DAOMedicinali;
import beans.Ricetta;
import beans.Paziente;
import beans.Medicinale;
import net.glxn.qrgen.QRCode;
import net.glxn.qrgen.image.ImageType;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;

@WebServlet(name = "DownloadRicettaServlet")
public class DownloadRicettaServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        DAORicette daoRicette = new DAORicette();
        Ricetta ricetta = daoRicette.getRicettaByID(Integer.parseInt(request.getParameter("rid")));
        DAOPazienti daoPazienti = new DAOPazienti();
        Paziente paziente = daoPazienti.getPazienteByID(ricetta.getPaziente());
        DAOMedicinali daoMedicinali = new DAOMedicinali();
        Medicinale medicinale = daoMedicinali.getMedicinaleByID(ricetta.getMedicinale());

        ServletOutputStream out = response.getOutputStream();
        String pdfRicetta = "ricetta-" + ricetta.getID() + ".pdf";
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + pdfRicetta);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        String qrInfo = ricetta.getMedico() + paziente.getCodiceFiscale() + ricetta.getDataPrescrizione().toString() +
                ricetta.getID() + medicinale.getDescrizione();
        byte[] qrBytes = QRCode.from(qrInfo).to(ImageType.JPG).stream().toByteArray();

        Document document = new Document();

        try {
            PdfWriter.getInstance(document, out);
            document.open();

            PdfPTable table = new PdfPTable(2);

            Font bold = new Font(Font.HELVETICA, 14, Font.BOLD);
            Font std = new Font(Font.HELVETICA, 10);

            PdfPCell cell = new PdfPCell();
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);

            Phrase p = new Phrase("Codice medico", bold);
            cell.setPhrase(p);
            table.addCell(cell);
            p = new Phrase(String.valueOf(ricetta.getMedico()), std);
            cell.setPhrase(p);
            table.addCell(cell);

            p = new Phrase("Codice fiscale paziente", bold);
            cell.setPhrase(p);
            table.addCell(cell);
            p = new Phrase(paziente.getCodiceFiscale(), std);
            cell.setPhrase(p);
            table.addCell(cell);

            p = new Phrase("Data prescrizione", bold);
            cell.setPhrase(p);
            table.addCell(cell);
            p = new Phrase(sdf.format(ricetta.getDataPrescrizione()), std);
            cell.setPhrase(p);
            table.addCell(cell);

            p = new Phrase("Codice univoco prescrizione", bold);
            cell.setPhrase(p);
            table.addCell(cell);
            p = new Phrase(String.valueOf(ricetta.getID()), std);
            cell.setPhrase(p);
            table.addCell(cell);

            p = new Phrase("Descrizione farmaco", bold);
            cell.setPhrase(p);
            table.addCell(cell);
            p = new Phrase(medicinale.getDescrizione(), std);
            cell.setPhrase(p);
            table.addCell(cell);

            document.add(table);

            Image qr = Image.getInstance(qrBytes);
            qr.setAlignment(Element.ALIGN_CENTER);
            document.add(qr);

            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        out.flush();
        out.close();
    }

}
