package services.paziente;

import beans.Ticket;
import com.lowagie.text.*;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import dao.DAOTickets;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@WebServlet(name = "DownloadTicketsServlet")
public class DownloadTicketsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOTickets daoTickets = new DAOTickets();
        ArrayList<Ticket> tickets = daoTickets.getTicketByIDPaziente(Integer.parseInt(session.getAttribute("ID").toString()));

        ServletOutputStream out = response.getOutputStream();
        String pdfReportTickets = "report-tickets-" + session.getAttribute("ID") + ".pdf";
        response.setContentType("application/pdf");
        response.setHeader("Content-Disposition", "attachment; filename=" + pdfReportTickets);

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, out);

            document.open();

            PdfPTable table = new PdfPTable(4);

            Font bold = new Font(Font.HELVETICA, 14, Font.BOLD);
            Font std = new Font(Font.HELVETICA, 10);

            PdfPCell cell = new PdfPCell();
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setVerticalAlignment(Element.ALIGN_CENTER);

            Phrase p = new Phrase("ID TICKET", bold);
            cell.setPhrase(p);
            table.addCell(cell);
            p = new Phrase("CAUSALE", bold);
            cell.setPhrase(p);
            table.addCell(cell);
            p = new Phrase("DATA", bold);
            cell.setPhrase(p);
            table.addCell(cell);
            p = new Phrase("IMPORTO [€]", bold);
            cell.setPhrase(p);
            table.addCell(cell);

            for (Ticket t : tickets) {
                p = new Phrase(String.valueOf(t.getID()), std);
                cell.setPhrase(p);
                table.addCell(cell);
                p = new Phrase(t.getCausale(), std);
                cell.setPhrase(p);
                table.addCell(cell);
                p = new Phrase(sdf.format(t.getData()), std);
                cell.setPhrase(p);
                table.addCell(cell);
                p = new Phrase(String.valueOf(t.getImporto()), std);
                cell.setPhrase(p);
                table.addCell(cell);
            }

            document.add(table);
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        }

        out.flush();
        out.close();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
