package services.general;

import beans.EsamePrescrivibile;
import dao.DAOListaEsami;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet("/models/lista-esami")
public class RestListaEsamiServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        PrintWriter out = response.getWriter();

        DAOListaEsami daoListaEsami = new DAOListaEsami();
        StringBuilder suggestion = new StringBuilder("[");
        ArrayList<EsamePrescrivibile> esamiPrescrivibili = daoListaEsami.getAllEsamiPrescrivibili();

        boolean first = true;
        for (EsamePrescrivibile ep : esamiPrescrivibili) {
            if (!first) {
                suggestion.append(",");
            } else {
                first = false;
            }
            suggestion.append("\"").append(ep.getNome()).append("\"");
        }
        suggestion.append("]");

        out.println(suggestion.toString());
        out.flush();
        out.close();
    }

}
