package services.general;

import beans.Esame;
import beans.EsamePrescrivibile;
import beans.Paziente;
import dao.DAOEsami;
import dao.DAOFoto;
import dao.DAOListaEsami;
import dao.DAOPazienti;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@WebServlet("/models/esami/*")
public class RestEsamiServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        DAOEsami daoEsami = new DAOEsami();
        StringBuilder suggestion;
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
            return;
        } else {
            String[] splits = pathInfo.split("/");

            // GET /models/esami/:idEsame
            if (splits.length == 2) {
//                String idEsame = splits[1];
                response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
                return;
            } else if (splits.length == 3 && splits[1].equals("paziente")) { // GET /models/esami/paziente/:idPaziente
                DAOListaEsami daoLE = new DAOListaEsami();
                DAOPazienti daoPazienti = new DAOPazienti();
                int idPaziente = Integer.parseInt(splits[2]);
                ArrayList<Esame> esami = daoEsami.getEsamiByIDPaziente(idPaziente);
                suggestion = new StringBuilder("[");
                boolean first = true;

                EsamePrescrivibile ep;
                for (Esame e : esami) {
                    ep = daoLE.getEsamePrescrivibiliByID(e.getEsamePrescrivibile());
                    if (!first) {
                        suggestion.append(",");
                    } else {
                        first = false;
                    }
                    suggestion.append("{\"id\":").append(e.getID()).append(",\"esame\":\"").append(ep.getNome())
                            .append("\",\"svolto\":").append(e.isSvolto()).append(",\"esito\":\"")
                            .append(e.getEsito()).append("\",\"data\":\"").append(e.getData()).append("\"}");
                }
                suggestion.append("]");

            } else {
                for (String s : splits) {
                    System.out.println(s);
                }
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
        }

        PrintWriter out = response.getWriter();
        out.println(suggestion.toString());
        out.flush();
        out.close();
    }

}
