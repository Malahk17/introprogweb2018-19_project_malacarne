package services.general;

import beans.Medicinale;
import beans.Paziente;
import dao.DAOMedicinali;
import dao.DAOPazienti;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

@WebServlet("/models/medicinali")
public class RestMedicinaliServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        DAOMedicinali daoMedicinali = new DAOMedicinali();
        ArrayList<Medicinale> medicinali = daoMedicinali.getAllMedicinali();
        StringBuilder suggestion = new StringBuilder("[");
        boolean first = true;

        for (Medicinale m : medicinali) {
            if (!first) {
                suggestion.append(",");
            } else {
                first = false;
            }
            suggestion.append("\"").append(m.getNome()).append("\"");
        }
        suggestion.append("]");

        PrintWriter out = response.getWriter();
        out.println(suggestion.toString());
        out.flush();
        out.close();
    }

}
