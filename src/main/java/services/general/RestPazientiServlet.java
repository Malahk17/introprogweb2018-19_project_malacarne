package services.general;

import beans.Foto;
import beans.Paziente;
import dao.DAOFoto;
import dao.DAOPazienti;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

@WebServlet("/models/pazienti/*")
public class RestPazientiServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");

        DAOPazienti daoPazienti = new DAOPazienti();
        StringBuilder suggestion;
        String pathInfo = request.getPathInfo();

        if (pathInfo == null || pathInfo.equals("/")) {
            ArrayList<Paziente> pazienti = daoPazienti.getAllPazienti();
            suggestion = new StringBuilder("[");
            boolean first = true;

            for (Paziente p : pazienti) {
                if (!first) {
                    suggestion.append(',');
                } else {
                    first = false;
                }
                suggestion.append("{\"id\":").append(p.getID()).append(",\"text\":\"").append(p.getCognome()).append(' ').append(p.getNome())
                        .append(' ').append(p.getCodiceFiscale()).append("\"}");
            }
            suggestion.append("]");
        } else {
            String[] splits = pathInfo.split("/");

            if (splits.length != 2) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }

            String idPaziente = splits[1];
            DAOFoto daoFoto = new DAOFoto();
            Paziente p = daoPazienti.getPazienteByID(Integer.parseInt(idPaziente));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            suggestion = new StringBuilder("{\"nome\":\"");
            suggestion.append(p.getNome());
            suggestion.append("\",\"cognome\":\"");
            suggestion.append(p.getCognome());
            suggestion.append("\",\"codice-fiscale\":\"");
            suggestion.append(p.getCodiceFiscale());
            suggestion.append("\",\"data-nascita\":\"");
            suggestion.append(sdf.format(p.getDataNascita()));
            suggestion.append("\",\"luogo-nascita\":\"");
            suggestion.append(p.getLuogoNascita());
            suggestion.append("\",\"foto\":\"");
            suggestion.append(daoFoto.getFotoByIDPazienteAttiva(p.getID()).getImgBase64Encoded());
            suggestion.append("\"}");
        }

        PrintWriter out = response.getWriter();
        out.println(suggestion.toString());
        out.flush();
        out.close();
    }

}
