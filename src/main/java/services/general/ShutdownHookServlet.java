package services.general;

import dao.DAOFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class ShutdownHookServlet implements ServletContextListener {

    public void contextDestroyed(ServletContextEvent sce) {
        DAOFactory.closeConnection();
    }

}