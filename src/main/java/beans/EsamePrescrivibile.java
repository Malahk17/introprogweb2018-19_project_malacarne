package beans;

import java.io.Serializable;

public class EsamePrescrivibile implements Serializable {
    private int ID;
    private String nome;
    private String descrizione;

    public EsamePrescrivibile() {
        ID = -1;
        nome = descrizione = "";
    }

    public EsamePrescrivibile(int ID, String nome, String descrizione) {
        this.ID = ID;
        this.nome = nome;
        this.descrizione = descrizione;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
}
