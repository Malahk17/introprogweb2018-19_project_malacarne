package beans;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.sql.Date;

public class Foto {
    private int idPaziente;
    private Date data;
    private byte[] immagine;
    private String imgBase64Encoded;

    public Foto() {
        idPaziente = -1;
        data = new Date(0);
        immagine = new byte[0];
        imgBase64Encoded = "";
    }

    public Foto(int idPaziente, Date data, byte[] immagine) {
        this.idPaziente = idPaziente;
        this.data = data;
        this.immagine = immagine;

        imgToBase64();
    }

    public int getIdPaziente() {
        return idPaziente;
    }

    public void setIdPaziente(int idPaziente) {
        this.idPaziente = idPaziente;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public byte[] getImmagine() {
        return immagine;
    }

    public void setImmagine(byte[] immagine) {
        this.immagine = immagine;
        imgToBase64();
    }

    public String getImgBase64Encoded() {
        return imgBase64Encoded;
    }

    private void imgToBase64() {
        byte[] encodeBase64 = Base64.encodeBase64(immagine);
        try {
            imgBase64Encoded = new String(encodeBase64, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            imgBase64Encoded = "";
            e.printStackTrace();
        }
    }
}
