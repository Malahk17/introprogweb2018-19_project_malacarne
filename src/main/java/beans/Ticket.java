package beans;

import java.io.Serializable;
import java.sql.Date;

public class Ticket implements Serializable {
    private int ID;
    private int paziente;
    private String causale;
    private Date data;
    private double importo;

    public Ticket() {
        ID = paziente = -1;
        causale = "";
        data = new Date(0);
        importo = 0.0;
    }

    public Ticket(int ID, int paziente, String causale, Date data, double importo) {
        this.ID = ID;
        this.paziente = paziente;
        this.causale = causale;
        this.data = data;
        this.importo = importo;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getPaziente() {
        return paziente;
    }

    public void setPaziente(int paziente) {
        this.paziente = paziente;
    }

    public String getCausale() {
        return causale;
    }

    public void setCausale(String causale) {
        this.causale = causale;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public double getImporto() {
        return importo;
    }

    public void setImporto(double importo) {
        this.importo = importo;
    }
}
