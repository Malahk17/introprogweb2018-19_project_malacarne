package beans;

import java.io.Serializable;
import java.sql.Date;

public class Ricetta implements Serializable {
    private int ID;
    private int medicinale;
    private int paziente;
    private int medico;
    private boolean ritirata;
    private Date dataPrescrizione;
    private int farmacia;

    public Ricetta() {
        ID = medicinale = paziente = medico = farmacia = -1;
        ritirata = false;
        dataPrescrizione = new Date(0);
    }

    public Ricetta(int ID, int medicinale, int paziente, int medico, boolean ritirata, Date dataPrescrizione, int farmacia) {
        this.ID = ID;
        this.medicinale = medicinale;
        this.paziente = paziente;
        this.medico = medico;
        this.ritirata = ritirata;
        this.dataPrescrizione = dataPrescrizione;
        this.farmacia = farmacia;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getMedicinale() {
        return medicinale;
    }

    public void setMedicinale(int medicinale) {
        this.medicinale = medicinale;
    }

    public int getPaziente() {
        return paziente;
    }

    public void setPaziente(int paziente) {
        this.paziente = paziente;
    }

    public int getMedico() {
        return medico;
    }

    public void setMedico(int medico) {
        this.medico = medico;
    }

    public boolean isRitirata() {
        return ritirata;
    }

    public void setRitirata(boolean ritirata) {
        this.ritirata = ritirata;
    }

    public Date getDataPrescrizione() {
        return dataPrescrizione;
    }

    public void setDataPrescrizione(Date dataPrescrizione) {
        this.dataPrescrizione = dataPrescrizione;
    }

    public int getFarmacia() {
        return farmacia;
    }

    public void setFarmacia(int farmacia) {
        this.farmacia = farmacia;
    }
}
