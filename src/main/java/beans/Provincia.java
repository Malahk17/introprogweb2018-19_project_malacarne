package beans;

public class Provincia {
    private String sigla;
    private String nome;

    public Provincia() {
        sigla = nome = "";
    }

    public Provincia(String sigla, String nome) {
        this.sigla = sigla;
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
