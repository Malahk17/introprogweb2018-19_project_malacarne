package beans;

import java.io.Serializable;

public class Farmacia implements Serializable {
    private int ID;
    private String nome;
    private String indirizzo;
    private String citta;
    private String provincia;

    public Farmacia() {
        ID = -1;
        nome = indirizzo = citta = provincia = "";
    }

    public Farmacia(int ID, String nome, String indirizzo, String citta, String provincia) {
        this.ID = ID;
        this.nome = nome;
        this.indirizzo = indirizzo;
        this.citta = citta;
        this.provincia = provincia;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
}
