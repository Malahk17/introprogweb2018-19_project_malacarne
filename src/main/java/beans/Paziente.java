package beans;

import java.io.Serializable;
import java.sql.Date;

public class Paziente implements Serializable {
    private int ID;
    private String codiceFiscale;
    private String nome;
    private String cognome;
    private Date dataNascita;
    private String luogoNascita;
    private char sesso;
    private int medicoBase;
    private String provincia;

    public Paziente() {
        ID = medicoBase = -1;
        codiceFiscale = luogoNascita = provincia = nome = cognome = "";
        dataNascita = new Date(0);
        sesso = '\0';
    }

    public Paziente(int ID, String codiceFiscale, String nome, String cognome, Date dataNascita, String luogoNascita, char sesso, int medicoBase, String provincia) {
        this.ID = ID;
        this.codiceFiscale = codiceFiscale;
        this.nome = nome;
        this.cognome = cognome;
        this.dataNascita = dataNascita;
        this.luogoNascita = luogoNascita;
        this.sesso = sesso;
        this.medicoBase = medicoBase;
        this.provincia = provincia;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getCodiceFiscale() {
        return codiceFiscale;
    }

    public void setCodiceFiscale(String codiceFiscale) {
        this.codiceFiscale = codiceFiscale;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public Date getDataNascita() {
        return dataNascita;
    }

    public void setDataNascita(Date dataNascita) {
        this.dataNascita = dataNascita;
    }

    public String getLuogoNascita() {
        return luogoNascita;
    }

    public void setLuogoNascita(String luogoNascita) {
        this.luogoNascita = luogoNascita;
    }

    public char getSesso() {
        return sesso;
    }

    public void setSesso(char sesso) {
        this.sesso = sesso;
    }

    public int getMedicoBase() {
        return medicoBase;
    }

    public void setMedicoBase(int medicoBase) {
        this.medicoBase = medicoBase;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }
}
