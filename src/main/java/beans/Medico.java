package beans;

import java.io.Serializable;
import java.sql.Date;

public class Medico implements Serializable {
    private int ID;
    private String nome;
    private String cognome;
    private boolean isBase;
    private String citta;
    private String provincia;
    private Date ultimaRicetta;
    private Date ultimaVisita;

    public Medico() {
        ID = -1;
        isBase = false;
        nome = cognome = citta = provincia = "";
        ultimaRicetta = ultimaVisita = new Date(0);
    }

    public Medico(int ID, String nome, String cognome, boolean isBase, String citta, String provincia, Date ultimaRicetta, Date ultimaVisita) {
        this.ID = ID;
        this.nome = nome;
        this.cognome = cognome;
        this.isBase = isBase;
        this.citta = citta;
        this.provincia = provincia;
        this.ultimaRicetta = ultimaRicetta;
        this.ultimaVisita = ultimaVisita;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public boolean isBase() {
        return isBase;
    }

    public void setBase(boolean base) {
        isBase = base;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public Date getUltimaRicetta() {
        return ultimaRicetta;
    }

    public void setUltimaRicetta(Date ultimaRicetta) {
        this.ultimaRicetta = ultimaRicetta;
    }

    public Date getUltimaVisita() {
        return ultimaVisita;
    }

    public void setUltimaVisita(Date ultimaVisita) {
        this.ultimaVisita = ultimaVisita;
    }
}
