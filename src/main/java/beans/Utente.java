package beans;

import java.io.Serializable;

public class Utente implements Serializable {
    private String email;
    private String pwdDigest;
    private String ruolo;
    private int IDUtente;

    public Utente() {
        email = pwdDigest = ruolo = "";
        IDUtente = -1;
    }

    public Utente(String email, String pwdDigest, String ruolo, int IDUtente) {
        this.email = email;
        this.pwdDigest = pwdDigest;
        this.ruolo = ruolo;
        this.IDUtente = IDUtente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwdDigest() {
        return pwdDigest;
    }

    public void setPwdDigest(String pwdDigest) {
        this.pwdDigest = pwdDigest;
    }

    public String getRuolo() {
        return ruolo;
    }

    public void setRuolo(String ruolo) {
        this.ruolo = ruolo;
    }

    public int getIDUtente() {
        return IDUtente;
    }

    public void setIDUtente(int IDUtente) {
        this.IDUtente = IDUtente;
    }
}
