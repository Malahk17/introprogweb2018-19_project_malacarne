package beans;

import java.io.Serializable;
import java.sql.Date;

public class Esame implements Serializable {
    private int ID;
    private int esamePrescrivibile;
    private int paziente;
    private boolean svolto;
    private String esito;
    private Date data;
    private boolean richiamo;
    private int medico;

    public Esame() {
        ID = esamePrescrivibile = paziente = medico = -1;
        svolto = richiamo = false;
        esito = "";
    }

    public Esame(int ID, int esamePrescrivibile, int paziente, boolean svolto, String esito, Date data, boolean richiamo, int medico) {
        this.ID = ID;
        this.esamePrescrivibile = esamePrescrivibile;
        this.paziente = paziente;
        this.svolto = svolto;
        this.esito = esito;
        this.data = data;
        this.richiamo = richiamo;
        this.medico = medico;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getEsamePrescrivibile() {
        return esamePrescrivibile;
    }

    public void setEsamePrescrivibile(int esamePrescrivibile) {
        this.esamePrescrivibile = esamePrescrivibile;
    }

    public int getPaziente() {
        return paziente;
    }

    public void setPaziente(int paziente) {
        this.paziente = paziente;
    }

    public boolean isSvolto() {
        return svolto;
    }

    public void setSvolto(boolean svolto) {
        this.svolto = svolto;
    }

    public String getEsito() {
        return esito;
    }

    public void setEsito(String esito) {
        this.esito = esito;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public boolean isRichiamo() {
        return richiamo;
    }

    public void setRichiamo(boolean richiamo) {
        this.richiamo = richiamo;
    }

    public int getMedico() {
        return medico;
    }

    public void setMedico(int medico) {
        this.medico = medico;
    }
}
