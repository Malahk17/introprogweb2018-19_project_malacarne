package login;

import beans.Utente;
import dao.DAOMedici;
import dao.DAOUtenti;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOUtenti daoUtenti = new DAOUtenti();
        Utente utente = daoUtenti.getUtenteByAuth(request.getParameter("email"), request.getParameter("password"));
        String ruolo = null, ID = null;

        if (utente != null) {
            ruolo = utente.getRuolo();

            if (ruolo.equals("SSP")) {
                // ssp@servizio.sanitario.~ SIGLA PROVINCIA ~.it
                // 23 caratteri - SIGLA - .it
                ID = utente.getEmail().substring(23, 25);
            } else if (!ruolo.equals("SSN")) {
                ID = String.valueOf(utente.getIDUtente());
            }

            String remember = request.getParameter("remember");
            if (remember != null && remember.equals("on")) {
                renewCookie(response,"ruolo", ruolo);
                if (ID != null) {
                    renewCookie(response, "ID", ID);
                }
            }

            session.setAttribute("ruolo", ruolo);
            if (!ruolo.equals("SSN")) {
                session.setAttribute("ID", ID);
            }
            switch (ruolo) {
                case "P":
                    response.sendRedirect(request.getContextPath() + "/cittadino-home.jsp");
                    break;
                case "M":
                    DAOMedici daoMedici = new DAOMedici();
                    if (daoMedici.getMedicoByID(Integer.parseInt(ID)).isBase()) {
                        response.sendRedirect(request.getContextPath() + "/mb-home.jsp");
                    } else {
                        response.sendRedirect(request.getContextPath() + "/ms-home.jsp");
                    }
                    break;
                case "F":
                    response.sendRedirect(request.getContextPath() + "/farmacia-home.jsp");
                    break;
                case "SSP":
                    response.sendRedirect(request.getContextPath() + "/ssp-home.jsp");
                    break;
                case "SSN":
                    response.sendRedirect(request.getContextPath() + "/ssn-home.jsp");
                    break;
            }
        } else {
            session.setAttribute("email", request.getParameter("email"));
            session.setAttribute("invalid-credentials", "");
            response.sendRedirect(request.getContextPath() + "/login.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(HttpServletResponse.SC_NOT_IMPLEMENTED);
    }

    private void renewCookie(HttpServletResponse res, String name, String value) {
        Cookie c = new Cookie(name, value);
        c.setMaxAge(60 * 60 * 24 * 3);

        res.addCookie(c);
    }

}
