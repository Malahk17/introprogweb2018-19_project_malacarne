package login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = "LogoutServlet")
public class LogoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logout(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        logout(request, response);
    }

    private void logout(HttpServletRequest req, HttpServletResponse res) throws IOException {
        HttpSession session = req.getSession();
        session.removeAttribute("ruolo");
        session.removeAttribute("ID");

        Cookie c1 = new Cookie("ruolo", "");
        Cookie c2 = new Cookie("ID", "");
        c1.setMaxAge(0);
        c2.setMaxAge(0);
        res.addCookie(c1);
        res.addCookie(c2);

        res.sendRedirect(req.getContextPath() + "/index.jsp");
    }
}
