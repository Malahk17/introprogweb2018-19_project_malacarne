package login;

import dao.DAOUtenti;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "CambiaPasswordServlet")
public class CambiaPasswordServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        DAOUtenti daoUtenti = new DAOUtenti();
        int id = Integer.parseInt(session.getAttribute("ID").toString());
        String ruolo = session.getAttribute("ruolo").toString();

        String pw1 = request.getParameter("new-pw");
        String pw2 = request.getParameter("rep-new-pw");
        if (pw1.equals(pw2)) {
            daoUtenti.updatePassword(id, ruolo, pw1);
        } else {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST);
            return;
        }

        response.sendRedirect(request.getHeader("referer"));
    }
}
