package login;

import dao.DAOMedici;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(filterName = "LoginFilter")
public class LoginFilter implements Filter {

    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        HttpSession session = request.getSession();
        String loginURI = request.getContextPath() + "/login.jsp";
        Cookie[] cookies = request.getCookies();

        // if heading to 'login.jsp'
        if (request.getRequestURI().equals(loginURI)) {
            String ruolo = null;
            String ID = null;

            for (Cookie c : cookies) {
                if (c.getName().equals("ruolo")) {
                    ruolo = c.getValue();
                } else if (c.getName().equals("ID")) {
                    ID = c.getValue();
                }
            }

            if (ruolo != null) {
                String redirect = redirectPath(ruolo);

                if (redirect != null) {
                    renewCookie(response, "ruolo", ruolo);
                    session.setAttribute("ruolo", ruolo);
                    if (!ruolo.equals("SSN")) {
                        renewCookie(response, "ID", ID);
                        session.setAttribute("ID", ID);
                    }

                    redirect = dispatcherMedico(redirect, ID);

                    response.sendRedirect(request.getContextPath() + redirect);
                }
            } else {
                ruolo = (String) session.getAttribute("ruolo");
                if (ruolo != null) {
                    String redirect = redirectPath(ruolo);

                    if (redirect != null) {
                        session.setAttribute("ruolo", ruolo);
                        if (!ruolo.equals("SSN")) {
                            ID = (String) session.getAttribute("ID");
                            session.setAttribute("ID", ID);
                        }

                        redirect = dispatcherMedico(redirect, ID);

                        response.sendRedirect(request.getContextPath() + redirect);
                    }
                }
            }
            chain.doFilter(req, resp);
        } else { // if heading to logged pages
            boolean redirectToLogin = true;

            for (Cookie c : cookies) {
                if (c.getName().equals("ruolo")) {
                    redirectToLogin = false;
                    session.setAttribute("ruolo", c.getValue());
                    for (Cookie k : cookies) {
                        if (k.getName().equals("ID")) {
                            session.setAttribute("ID", k.getValue());
                        }
                    }
                }
            }

            if (session.getAttribute("ruolo") != null) {
                redirectToLogin = false;
            }

            if (redirectToLogin) {
                response.sendRedirect(request.getContextPath() + "/login.jsp");
            } else {
                chain.doFilter(req, resp);
            }
        }
    }

    public void init(FilterConfig config) throws ServletException {

    }

    private void renewCookie(HttpServletResponse res, String name, String value) {
        Cookie c = new Cookie(name, value);
        c.setMaxAge(60 * 60 * 24 * 3);

        res.addCookie(c);
    }

    private String redirectPath(String ruolo) throws IOException {
        String redirect = null;

        switch (ruolo) {
            case "P":
                redirect = "/cittadino-home.jsp";
                break;
            case "M":
                redirect = "";
                break;
            case "F":
                redirect = "/farmacia-home.jsp";
                break;
            case "SSP":
                redirect = "/ssp-home.jsp";
                break;
            case "SSN":
                redirect = "/ssn-home.jsp";
                break;
        }

        return redirect;
    }

    private String dispatcherMedico(String redirect, String ID) {
        if (redirect.equals("")) {
            DAOMedici daoMedici = new DAOMedici();
            if (daoMedici.getMedicoByID(Integer.parseInt(ID)).isBase()) {
                redirect = "/mb-home.jsp";
            } else {
                redirect = "/ms-home.jsp";
            }
        }
        return redirect;
    }

}
