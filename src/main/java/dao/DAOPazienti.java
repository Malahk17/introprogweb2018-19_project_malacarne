package dao;

import beans.Paziente;

import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

public class DAOPazienti {

    public ArrayList<Paziente> getAllPazienti() {
        ArrayList<Paziente> pazienti = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Pazienti";
        ResultSet results = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.execute();

            results = preparedStatement.getResultSet();
            while (results != null && results.next()) {
                pazienti.add(
                        new Paziente(results.getInt(1), results.getString(2), results.getString(3),
                                results.getString(4), results.getDate(5), results.getString(6),
                                results.getString(7).charAt(0), results.getInt(8), results.getString(9))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                results.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return pazienti;
    }

    public Paziente getPazienteByID(int ID) {
        Paziente paziente = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Pazienti WHERE ID=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, ID);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result != null && result.next()) {
                paziente = new Paziente(result.getInt(1), result.getString(2), result.getString(3),
                        result.getString(4), result.getDate(5), result.getString(6),
                        result.getString(7).charAt(0), result.getInt(8),
                        result.getString(9));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return paziente;
    }

    public Paziente getPazienteByCodiceFiscale(String codiceFiscale) {
        Paziente paziente = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Pazienti WHERE CodFiscale=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, codiceFiscale);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result != null && result.next()) {
                paziente = new Paziente(
                        result.getInt(1), result.getString(2), result.getString(3),
                        result.getString(4), result.getDate(5), result.getString(6),
                        result.getString(7).charAt(0), result.getInt(8), result.getString(9)
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return paziente;
    }

    public ArrayList<Paziente> getPazientiByMedico(int idMedico) {
        ArrayList<Paziente> pazienti = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Pazienti WHERE medicob=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idMedico);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                pazienti.add(
                        new Paziente(result.getInt(1), result.getString(2), result.getString(3),
                                result.getString(4), result.getDate(5), result.getString(6),
                                result.getString(7).charAt(0), result.getInt(8), result.getString(9))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return pazienti;
    }

    public ArrayList<Paziente> getPazientiByProvincia(String provincia) {
        ArrayList<Paziente> pazienti = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Pazienti WHERE provincia=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, provincia.toUpperCase());
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                pazienti.add(
                        new Paziente(result.getInt(1), result.getString(2), result.getString(3),
                                result.getString(4), result.getDate(5), result.getString(6),
                                result.getString(7).charAt(0), result.getInt(8), result.getString(9))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return pazienti;
    }

    public ArrayList<Paziente> getPazientiByProvinciaMin(String provincia, int min) {
        ArrayList<Paziente> pazienti = getPazientiByProvincia(provincia);
        Calendar pDate = Calendar.getInstance();
        Calendar minDate = Calendar.getInstance();
        minDate.setTime(new Date(System.currentTimeMillis()));
        minDate.add(Calendar.YEAR, -min);

        Iterator<Paziente> it = pazienti.iterator();
        while (it.hasNext()) {
            Paziente p = it.next();
            pDate.setTime(p.getDataNascita());
            if (pDate.after(minDate)) {
                it.remove();
            }
        }

        return pazienti;
    }

    public ArrayList<Paziente> getPazientiByProvinciaMax(String provincia, int max) {
        ArrayList<Paziente> pazienti = getPazientiByProvincia(provincia);
        Calendar pDate = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        maxDate.setTime(new Date(System.currentTimeMillis()));
        maxDate.add(Calendar.YEAR, -max);

        Iterator<Paziente> it = pazienti.iterator();
        while (it.hasNext()) {
            Paziente p = it.next();
            pDate.setTime(p.getDataNascita());
            if (pDate.before(maxDate)) {
                it.remove();
            }
        }

        return pazienti;
    }

    public ArrayList<Paziente> getPazientiByProvinciaMinMax(String provincia, int min, int max) {
        ArrayList<Paziente> pazienti = getPazientiByProvincia(provincia);

        Calendar pDate = Calendar.getInstance();
        Calendar maxDate = Calendar.getInstance();
        maxDate.setTime(new Date(System.currentTimeMillis()));
        maxDate.add(Calendar.YEAR, -max);

        Calendar minDate = Calendar.getInstance();
        minDate.setTime(new Date(System.currentTimeMillis()));
        minDate.add(Calendar.YEAR, -min);

        Iterator<Paziente> it = pazienti.iterator();
        while (it.hasNext()) {
            Paziente p = it.next();
            pDate.setTime(p.getDataNascita());
            if (pDate.after(minDate) || pDate.before(maxDate)) {
                it.remove();
            }
        }

        return pazienti;
    }

    public void updateMedico(int idPaziente, int idNuovoMedico) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "UPDATE Pazienti SET Medicob=? WHERE ID=?";

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idNuovoMedico);
            preparedStatement.setInt(2, idPaziente);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
