package dao;

import beans.EsamePrescrivibile;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAOListaEsami {

    public ArrayList<EsamePrescrivibile> getAllEsamiPrescrivibili() {
        ArrayList<EsamePrescrivibile> esamiPrescrivibili = new ArrayList<>();
        EsamePrescrivibile esamePrescrivibile = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM ListaEsami";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                esamePrescrivibile = new EsamePrescrivibile(result.getInt(1), result.getString(2),
                        result.getString(3));
                esamiPrescrivibili.add(esamePrescrivibile);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return esamiPrescrivibili;
    }

    public EsamePrescrivibile getEsamePrescrivibiliByID(int ID) {
        EsamePrescrivibile esame = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM ListaEsami WHERE ID=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, ID);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                esame = new EsamePrescrivibile(result.getInt(1), result.getString(2),
                        result.getString(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return esame;
    }

    public int getIdEsamePrescrivibileByNome(String nome) {
        EsamePrescrivibile esame = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM ListaEsami WHERE nome=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, nome);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                esame = new EsamePrescrivibile(result.getInt(1), result.getString(2),
                        result.getString(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return (esame != null ? esame.getID() : -1);
    }

}
