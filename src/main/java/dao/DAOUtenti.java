package dao;

import beans.Utente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAOUtenti {

    public Utente getUtenteByAuth(String email, String pwd) {
        Utente utente = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Utenti WHERE Email=? AND PwdDigest=md5(?)";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, pwd);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result != null && result.next()) {
                utente = new Utente(result.getString(1), result.getString(2), result.getString(3).trim(),
                        result.getInt(4));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return utente;
    }

    public String getMailPaziente(int idPaziente) {
        String mail = null;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "SELECT email FROM Utenti WHERE ruolo='P' AND idutente=?";
        ResultSet rs = null;

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setInt(1, idPaziente);
            ps.execute();

            rs = ps.getResultSet();
            if (rs.next()) {
                mail = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return mail;
    }

    public void updatePassword(int id, String ruolo, String password) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "UPDATE Utenti SET pwddigest=md5(?) WHERE idutente=? AND ruolo=?";

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setString(1, password);
            ps.setInt(2, id);
            ps.setString(3, ruolo);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
