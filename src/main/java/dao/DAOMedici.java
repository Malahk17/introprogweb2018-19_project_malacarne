package dao;

import beans.Medico;

import java.sql.*;
import java.util.ArrayList;

public class DAOMedici {

    public Medico getMedicoByID(int ID) {
        Medico medico = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Medici WHERE ID=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, ID);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                medico = new Medico(result.getInt(1), result.getString(2), result.getString(3),
                        result.getBoolean(4), result.getString(5), result.getString(6), result.getDate(7), result.getDate(8));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return medico;
    }

    public Medico getMedicoByID(int ID, boolean isBase) {
        Medico medico = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Medici WHERE ID=? AND isbase=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, ID);
            preparedStatement.setBoolean(2, isBase);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                medico = new Medico(result.getInt(1), result.getString(2), result.getString(3),
                        result.getBoolean(4), result.getString(5), result.getString(6), result.getDate(7), result.getDate(8));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return medico;
    }

    public Medico getMedicoByCognomeNome(String cognomenome) {
        Medico medico = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM (SELECT *, concat(cognome, ' ', nome) as cognomenome FROM Medici) as CN WHERE cognomenome=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, cognomenome);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                medico = new Medico(result.getInt(1), result.getString(2), result.getString(3),
                        result.getBoolean(4), result.getString(5), result.getString(6), result.getDate(7), result.getDate(8));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return medico;
    }

    public ArrayList<Medico> getMediciByProvincia(String provincia) {
        ArrayList<Medico> medici = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Medici WHERE Provincia=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, provincia);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                medici.add( new Medico(result.getInt(1), result.getString(2), result.getString(3),
                        result.getBoolean(4), result.getString(5), result.getString(6), result.getDate(7), result.getDate(8)) );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return medici;
    }

    public ArrayList<Medico> getMediciByProvincia(String provincia, boolean isBase) {
        ArrayList<Medico> medici = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Medici WHERE Provincia=? AND isBase=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, provincia);
            preparedStatement.setBoolean(2, isBase);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                medici.add( new Medico(result.getInt(1), result.getString(2), result.getString(3),
                        result.getBoolean(4), result.getString(5), result.getString(6), result.getDate(7), result.getDate(8)) );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return medici;
    }

    public void updateUltimaRicetta(int idMedico) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "UPDATE Medici SET ultimaricetta=? WHERE id=?";

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setDate(1, new Date(System.currentTimeMillis()));
            ps.setInt(2, idMedico);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void updateUltimaVisita(int idMedico) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "UPDATE Medici SET ultimavisita=? WHERE id=?";

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setDate(1, new Date(System.currentTimeMillis()));
            ps.setInt(2, idMedico);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
