package dao;

import beans.Esame;
import beans.Paziente;

import java.sql.*;
import java.util.ArrayList;

public class DAOEsami {

    public Esame getEsameByID(int idEsame) {
        Esame esame = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Esami WHERE id=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idEsame);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                esame = new Esame(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getBoolean(4), result.getString(5), result.getDate(6),
                        result.getBoolean(7), result.getInt(8));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return esame;
    }

    public ArrayList<Esame> getEsamiByIDPaziente(int IDpaziente) {
        ArrayList<Esame> esami = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Esami WHERE paziente=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, IDpaziente);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                esami.add(new Esame(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getBoolean(4), result.getString(5), result.getDate(6),
                        result.getBoolean(7), result.getInt(8)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return esami;
    }

    public ArrayList<Esame> getEsamiByIDPaziente(int IDpaziente, boolean svolti) {
        ArrayList<Esame> esami = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Esami WHERE paziente=? AND Svolto=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, IDpaziente);
            preparedStatement.setBoolean(2, svolti);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                esami.add(new Esame(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getBoolean(4), result.getString(5), result.getDate(6),
                        result.getBoolean(7), result.getInt(8)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return esami;
    }

    public ArrayList<Esame> getEsamiBySvolto(boolean svolti) {
        ArrayList<Esame> esami = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Esami WHERE Svolto=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setBoolean(1, svolti);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                esami.add(new Esame(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getBoolean(4), result.getString(5), result.getDate(6),
                        result.getBoolean(7), result.getInt(8)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return esami;
    }

    public ArrayList<Esame> getEsamiBySvoltoProvincia(boolean svolti, String provincia) {
        ArrayList<Esame> esami = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Esami WHERE Svolto=? AND paziente IN (SELECT id FROM Pazienti WHERE provincia=?)";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setBoolean(1, svolti);
            preparedStatement.setString(2, provincia.toUpperCase());
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                esami.add(new Esame(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getBoolean(4), result.getString(5), result.getDate(6),
                        result.getBoolean(7), result.getInt(8)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return esami;
    }

    public boolean hasPazienteEsame(int idPaziente, int idEsamePrescrivibile) {
        boolean has = false;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "SELECT * FROM Esami WHERE paziente=? AND esame=? AND svolto=true";
        ResultSet rs = null;

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setInt(1, idPaziente);
            ps.setInt(2, idEsamePrescrivibile);
            ps.execute();

            rs = ps.getResultSet();
            if (rs.next()) {
                has = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return has;
    }

    public void erogaEsame(int id, String esito, int medico) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "UPDATE Esami SET svolto=true, esito=?, data=?, medico=? WHERE id=?";

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setString(1, esito);
            ps.setDate(2, new Date(System.currentTimeMillis()));
            ps.setInt(3, medico);
            ps.setInt(4, id);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void addEsame(int idPaziente, int idEsame) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "INSERT INTO Esami (esame, paziente) VALUES (?, ?)";

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setInt(1, idEsame);
            ps.setInt(2, idPaziente);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public ArrayList<Paziente> addRichiamo(String nomeEsame, String provincia) {
        ArrayList<Paziente> pazientiRichiamati = new ArrayList<>();
        DAOListaEsami daoListaEsami = new DAOListaEsami();
        DAOPazienti daoPazienti = new DAOPazienti();

        Connection conn = null;
        PreparedStatement ps = null;
        String query = "INSERT INTO Esami (esame, paziente, richiamo) VALUES (?, ?, true)";
        ResultSet rs = null;

        int idEsame = daoListaEsami.getIdEsamePrescrivibileByNome(nomeEsame);
        ArrayList<Paziente> pazienti = daoPazienti.getPazientiByProvincia(provincia);

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);

            ps.setInt(1, idEsame);

            for (Paziente p : pazienti) {
                if (hasPazienteEsame(p.getID(), idEsame)) {
                    ps.setInt(2, p.getID());
                    ps.addBatch();
                    pazientiRichiamati.add(p);
                }
            }

            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return pazientiRichiamati;
    }

    public ArrayList<Paziente> addRichiamoMin(String nomeEsame, String provincia, int min) {
        ArrayList<Paziente> pazientiRichiamati = new ArrayList<>();
        DAOListaEsami daoListaEsami = new DAOListaEsami();
        DAOPazienti daoPazienti = new DAOPazienti();

        Connection conn = null;
        PreparedStatement ps = null;
        String query = "INSERT INTO Esami (esame, paziente, richiamo) VALUES (?, ?, true)";
        ResultSet rs = null;

        int idEsame = daoListaEsami.getIdEsamePrescrivibileByNome(nomeEsame);
        ArrayList<Paziente> pazienti = daoPazienti.getPazientiByProvinciaMin(provincia, min);

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);

            ps.setInt(1, idEsame);

            for (Paziente p : pazienti) {
                if (hasPazienteEsame(p.getID(), idEsame)) {
                    ps.setInt(2, p.getID());
                    ps.addBatch();
                    pazientiRichiamati.add(p);
                }
            }

            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return pazientiRichiamati;
    }

    public ArrayList<Paziente> addRichiamoMax(String nomeEsame, String provincia, int max) {
        ArrayList<Paziente> pazientiRichiamati = new ArrayList<>();
        DAOListaEsami daoListaEsami = new DAOListaEsami();
        DAOPazienti daoPazienti = new DAOPazienti();

        Connection conn = null;
        PreparedStatement ps = null;
        String query = "INSERT INTO Esami (esame, paziente, richiamo) VALUES (?, ?, true)";
        ResultSet rs = null;

        int idEsame = daoListaEsami.getIdEsamePrescrivibileByNome(nomeEsame);
        ArrayList<Paziente> pazienti = daoPazienti.getPazientiByProvinciaMax(provincia, max);

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);

            ps.setInt(1, idEsame);

            for (Paziente p : pazienti) {
                if (hasPazienteEsame(p.getID(), idEsame)) {
                    ps.setInt(2, p.getID());
                    ps.addBatch();
                    pazientiRichiamati.add(p);
                }
            }

            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return pazientiRichiamati;
    }

    public ArrayList<Paziente> addRichiamoMinMax(String nomeEsame, String provincia, int min, int max) {
        ArrayList<Paziente> pazientiRichiamati = new ArrayList<>();
        DAOListaEsami daoListaEsami = new DAOListaEsami();
        DAOPazienti daoPazienti = new DAOPazienti();

        Connection conn = null;
        PreparedStatement ps = null;
        String query = "INSERT INTO Esami (esame, paziente, richiamo) VALUES (?, ?, true)";
        ResultSet rs = null;

        int idEsame = daoListaEsami.getIdEsamePrescrivibileByNome(nomeEsame);
        ArrayList<Paziente> pazienti = daoPazienti.getPazientiByProvinciaMinMax(provincia, min, max);

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);

            ps.setInt(1, idEsame);

            for (Paziente p : pazienti) {
                if (hasPazienteEsame(p.getID(), idEsame)) {
                    ps.setInt(2, p.getID());
                    ps.addBatch();
                    pazientiRichiamati.add(p);
                }
            }

            ps.executeBatch();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return pazientiRichiamati;
    }

}
