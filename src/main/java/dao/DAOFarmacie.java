package dao;

import beans.Farmacia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DAOFarmacie {

    public Farmacia getFarmaciaByID(int idFarmacia) {
        Farmacia farmacia = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Farmacie WHERE id=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idFarmacia);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                farmacia = new Farmacia(result.getInt(1), result.getString(2), result.getString(3),
                        result.getString(4), result.getString(5));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return farmacia;
    }
}
