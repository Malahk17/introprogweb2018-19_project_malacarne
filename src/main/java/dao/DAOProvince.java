package dao;

import beans.Provincia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAOProvince {

    public Provincia getProvinciaByNome(String nome) {
        Provincia provincia = null;
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "SELECT * FROM Province WHERE provincia=?";
        ResultSet rs = null;

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setString(1, nome);
            ps.execute();

            rs = ps.getResultSet();
            if (rs.next()) {
                provincia = new Provincia(rs.getString(1), rs.getString(2));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return provincia;
    }

    public ArrayList<Provincia> getAllProvince() {
        ArrayList<Provincia> province = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "SELECT * FROM Province";
        ResultSet rs = null;

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.execute();

            rs = ps.getResultSet();
            while (rs.next()) {
                province.add(
                        new Provincia(rs.getString(1), rs.getString(2))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return province;
    }

}
