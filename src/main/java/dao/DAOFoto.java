package dao;

import beans.Foto;

import java.sql.*;
import java.util.ArrayList;

public class DAOFoto {

    public ArrayList<Foto> getFotoByIDPaziente(int idPaziente) {
        ArrayList<Foto> foto = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Foto WHERE paziente=? AND attiva=false";
        ResultSet results = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idPaziente);
            preparedStatement.execute();

            results = preparedStatement.getResultSet();
            while (results.next()) {
                foto.add(new Foto(results.getInt(1), results.getDate(2), results.getBytes(3)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                results.close();
            } catch (NullPointerException | SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (NullPointerException | SQLException e) {
                e.printStackTrace();
            }
        }

        return foto;
    }

    public Foto getFotoByIDPazienteAttiva(int idPaziente) {
        Foto foto = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Foto WHERE paziente=? AND attiva=true";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idPaziente);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                foto = new Foto(result.getInt(1), result.getDate(2), result.getBytes(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (NullPointerException | SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (NullPointerException | SQLException e) {
                e.printStackTrace();
            }
        }

        return foto;
    }

    public Foto getFotoByIDPazienteData(int idPaziente, Date data) {
        Foto foto = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Foto WHERE paziente=? AND data=?";
        ResultSet results = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idPaziente);
            preparedStatement.setDate(2, data);
            preparedStatement.execute();

            results = preparedStatement.getResultSet();
            if (results.next()) {
                foto = new Foto(results.getInt(1), results.getDate(2), results.getBytes(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                results.close();
            } catch (NullPointerException | SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (NullPointerException | SQLException e) {
                e.printStackTrace();
            }
        }

        return foto;
    }

    public void insertFoto(int idPaziente, Date data, byte[] immagine) {
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "INSERT INTO Foto VALUES (?, ?, ?, true)";

        disattivaFoto(idPaziente);

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idPaziente);
            preparedStatement.setDate(2, data);
            preparedStatement.setBytes(3, immagine);
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                preparedStatement.close();
            } catch (NullPointerException | SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void disattivaFoto(int idPaziente) {
        if (!getFotoByIDPaziente(idPaziente).isEmpty()) {
            Connection conn = null;
            PreparedStatement preparedStatement = null;
            String query = "UPDATE Foto SET attiva=false WHERE paziente=? AND attiva=true";

            try {
                conn = DAOFactory.getConnection();

                preparedStatement = conn.prepareStatement(query);
                preparedStatement.setInt(1, idPaziente);
                preparedStatement.executeUpdate();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    preparedStatement.close();
                } catch (NullPointerException | SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
