package dao;

import beans.Ricetta;

import java.sql.*;
import java.util.ArrayList;

public class DAORicette {

    public Ricetta getRicettaByID(int idRicetta) {
        Ricetta ricetta = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Ricette WHERE id=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, idRicetta);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                ricetta = new Ricetta(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getInt(4), result.getBoolean(5), result.getDate(6), result.getInt(7));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ricetta;
    }

    public ArrayList<Ricetta> getRicetteByIDPaziente(int IDpaziente, boolean ritirate) {
        ArrayList<Ricetta> ricette = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Ricette WHERE paziente=? AND ritirata=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, IDpaziente);
            preparedStatement.setBoolean(2, ritirate);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                ricette.add(new Ricetta(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getInt(4), result.getBoolean(5), result.getDate(6), result.getInt(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ricette;
    }

    public ArrayList<Ricetta> getRicetteByIDMedico(int IDmedico, boolean ritirate) {
        ArrayList<Ricetta> ricette = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Ricette WHERE medico=? AND ritirata=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, IDmedico);
            preparedStatement.setBoolean(2, ritirate);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                ricette.add(new Ricetta(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getInt(4), result.getBoolean(5), result.getDate(6), result.getInt(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ricette;
    }

    public ArrayList<Ricetta> getRicetteByProvinciaData(String provincia, Date data, boolean ritirate) {
        ArrayList<Ricetta> ricette = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Ricette WHERE ritirata=? AND dataprescrizione=? AND paziente IN (SELECT id FROM Pazienti WHERE provincia=?)";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setBoolean(1, ritirate);
            preparedStatement.setDate(2, data);
            preparedStatement.setString(3, provincia.toUpperCase());
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                ricette.add(new Ricetta(result.getInt(1), result.getInt(2), result.getInt(3),
                        result.getInt(4), result.getBoolean(5), result.getDate(6), result.getInt(7)));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return ricette;
    }

    public void setRicettaRitirata(int idRicetta, int idFarmacia) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "UPDATE Ricette SET ritirata=true, farmacia=? WHERE id=?";

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setInt(1, idFarmacia);
            ps.setInt(2, idRicetta);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void addRicetta(int idPaziente, int idMedicinale, int idMedico) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "INSERT INTO Ricette (medicinale, paziente, medico, dataprescrizione) VALUES (?, ?, ?, ?)";

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setInt(1, idMedicinale);
            ps.setInt(2, idPaziente);
            ps.setInt(3, idMedico);
            ps.setDate(4, new Date(System.currentTimeMillis()));
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            }catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
