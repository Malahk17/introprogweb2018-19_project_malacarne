package dao;

import beans.Medicinale;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAOMedicinali {

    public Medicinale getMedicinaleByID(int ID) {
        Medicinale medicinale = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Medicinali WHERE ID=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, ID);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                medicinale = new Medicinale(result.getInt(1), result.getString(2), result.getString(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return medicinale;
    }

    public Medicinale getMedicinaleByNome(String nome) {
        Medicinale medicinale = null;
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Medicinali WHERE nome=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setString(1, nome);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            if (result.next()) {
                medicinale = new Medicinale(result.getInt(1), result.getString(2), result.getString(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return medicinale;
    }

    public ArrayList<Medicinale> getAllMedicinali() {
        ArrayList<Medicinale> medicinali = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Medicinali";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                medicinali.add(
                        new Medicinale(result.getInt(1), result.getString(2), result.getString(3))
                );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return medicinali;
    }

}
