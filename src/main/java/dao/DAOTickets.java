package dao;

import beans.Ticket;

import java.sql.*;
import java.util.ArrayList;

public class DAOTickets {

    public ArrayList<Ticket> getTicketByIDPaziente(int IDpaziente) {
        ArrayList<Ticket> tickets = new ArrayList<>();
        Connection conn = null;
        PreparedStatement preparedStatement = null;
        String query = "SELECT * FROM Tickets WHERE paziente=?";
        ResultSet result = null;

        try {
            conn = DAOFactory.getConnection();

            preparedStatement = conn.prepareStatement(query);
            preparedStatement.setInt(1, IDpaziente);
            preparedStatement.execute();

            result = preparedStatement.getResultSet();
            while (result.next()) {
                tickets.add( new Ticket(result.getInt(1), result.getInt(2), result.getString(3),
                        result.getDate(4), result.getDouble(5)) );
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                result.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        return tickets;
    }

    public void addTicket(int idPaziente, String causale, double importo) {
        Connection conn = null;
        PreparedStatement ps = null;
        String query = "INSERT INTO Tickets(Paziente, Causale, Data, Importo) VALUES (?, ?, ?, ?)";

        try {
            conn = DAOFactory.getConnection();

            ps = conn.prepareStatement(query);
            ps.setInt(1, idPaziente);
            ps.setString(2, causale);
            ps.setDate(3, new Date(System.currentTimeMillis()));
            ps.setDouble(4, importo);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                ps.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

}
