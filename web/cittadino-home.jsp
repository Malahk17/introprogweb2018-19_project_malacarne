<%--
  Created by IntelliJ IDEA.
  Author: Paolo Malacarne
--%>
<%@ page session="true"
         contentType="text/html;charset=UTF-8"
         language="java" %>

<%@ page import="beans.*" %>
<%@ page import="dao.*" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.ArrayList" %>

<%
    String serverPath = request.getRequestURL().toString().replaceFirst(request.getRequestURI(), "") + request.getContextPath();

    DAOEsami daoEsami = new DAOEsami();
    DAOListaEsami daoListaEsami = new DAOListaEsami();
    DAOMedici daoMedici = new DAOMedici();
    DAORicette daoRicette = new DAORicette();
    DAOMedicinali daoMedicinali = new DAOMedicinali();
    DAOTickets daoTickets = new DAOTickets();
    DAOPazienti daoPazienti = new DAOPazienti();
    DAOFoto daoFoto = new DAOFoto();

    String ID = (String) session.getAttribute("ID");
    if (ID == null) {
        for (Cookie c : request.getCookies()) {
            if (c.getName().equals("ID")) {
                ID = c.getValue();
            }
        }
    }
    int IDpaziente = Integer.parseInt(ID);

    Paziente paziente = daoPazienti.getPazienteByID(IDpaziente);
    Foto profilo = daoFoto.getFotoByIDPazienteAttiva(IDpaziente);
    if (profilo == null) {
        profilo = new Foto();
    }
    ArrayList<Foto> foto = daoFoto.getFotoByIDPaziente(IDpaziente);

    ArrayList<Esame> esami;
    ArrayList<Ricetta> ricette;
    ArrayList<Ticket> tickets;

    EsamePrescrivibile ep;
    Medico medico;
    Medicinale medicinale;

    int idMedico = paziente.getMedicoBase();

    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
%>
<html>
<head>
    <title>Sistema Sanitario - Cittadino</title>
    <link rel="icon" type="image/ico" href="imgs/stellone.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTable CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          integrity="sha256-nbyata2PJRjImhByQzik2ot6gSHSU4Cqdz5bNYL2zcU=" crossorigin="anonymous"/>

    <!-- Javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- DataTable js -->
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script language="JavaScript">
        $(document).ready(function () {

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
            });
            $('table.my-datatable').DataTable({
                "scrollY": "250px",
                "scrollCollapse": false,
                "paging": false,
                "info": false,
                "language": {
                    "search": "Cerca:",
                    "emptyTable": "Non ci sono dati da visualizzare.",
                    "zeroRecords": "Nessun risultato trovato"
                }
            });
            $('.dataTables_length').addClass('bs-select');

            let url = '<%= serverPath %>/CambioMedicoServlet';
            console.log(url);
            $.getJSON(url, function (data) {
                $('#nuovo-medico').select2({
                    allowClear: true,
                    data: data,
                    placeholder: "Seleziona medico",
                    theme: "bootstrap"
                });
            });

            $('.custom-file-input').on('change', function () {
                let fileName = $(this).val().split('\\').pop();
                $(this).siblings('.custom-file-label').addClass('selected').html(fileName);
                $('#submit-immagine').removeAttr('disabled');
            });
        });
    </script>
</head>
<body class="text-center">

<!-- HEADER -->
<%@include file="header-logged.jsp" %>

<!-- TABS -->
<div class="container">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#esami">Esami</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#ricette">Ricette</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#ticket">Ticket</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#cambia-medico">Cambia medico</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#foto">Foto</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#cambia-pw">Cambia password</a>
        </li>
    </ul>

    <br/>
    <br/>

    <!-- Tab panes -->
    <div class="tab-content">

        <div id="esami" class="container tab-pane active">

            <h3>Lista esami a cui sottoporsi</h3>
            <table class="table table-striped my-datatable" style="text-align: center;">
                <thead>
                <tr>
                    <th class="th-sm">ID Esame</th>
                    <th class="th-sm">Esame</th>
                    <th class="th-sm">Descrizione (esame)</th>
                    <th class="th-sm">[Richiamo]</th>
                </tr>
                </thead>
                <tbody>
                <%
                    esami = daoEsami.getEsamiByIDPaziente(IDpaziente, false);
                    for (Esame e : esami) {
                        ep = daoListaEsami.getEsamePrescrivibiliByID(e.getEsamePrescrivibile());

                        if (e.isRichiamo()) {
                            out.println("<tr>" +
                                    "<td>" + e.getID() + "</td>" +
                                    "<td>" + ep.getNome() + "</td>" +
                                    "<td>" + ep.getDescrizione() + "</td>" +
                                    "<td><div class='alert alert-warning' role='alert'>Richiamo</div></tr>" +
                                    "</tr");
                        } else {
                            out.println("<tr>" +
                                    "<td>" + e.getID() + "</td>" +
                                    "<td>" + ep.getNome() + "</td>" +
                                    "<td>" + ep.getDescrizione() + "</td>" +
                                    "<td></tr>" +
                                    "</tr");
                        }
                    }
                %>
                </tbody>
            </table>

            <br/>
            <br/>

            <h3>Storico esami</h3>
            <table class="table table-striped my-datatable" style="text-align: center;">
                <thead>
                <tr>
                    <th class="th-sm">ID Esame</th>
                    <th class="th-sm">Esame</th>
                    <th class="th-sm">Data</th>
                    <th class="th-sm">Esito</th>
                </tr>
                </thead>
                <tbody>
                <%
                    esami = daoEsami.getEsamiByIDPaziente(IDpaziente, true);
                    for (Esame e : esami) {
                        ep = daoListaEsami.getEsamePrescrivibiliByID(e.getEsamePrescrivibile());
                        out.println("<tr>" +
                                "<td>" + e.getID() + "</td>" +
                                "<td>" + ep.getNome() + "</td>" +
                                "<td>" + e.getData() + "</td>" +
                                "<td>" + e.getEsito() + "</td>" +
                                "</tr");
                    }
                %>
                </tbody>
            </table>
        </div>

        <div id="ricette" class="container tab-pane fade">

            <h3>Ricette da ritirare</h3>
            <table class="table table-striped my-datatable" style="text-align: center;">
                <thead>
                <tr>
                    <th class="th-sm">ID Ricetta</th>
                    <th class="th-sm">Medicinale</th>
                    <th class="th-sm">Medico prescrivente</th>
                    <th class="th-sm"></th>
                </tr>
                </thead>
                <tbody>
                <%
                    ricette = daoRicette.getRicetteByIDPaziente(IDpaziente, false);
                    for (Ricetta r : ricette) {
                        medicinale = daoMedicinali.getMedicinaleByID(r.getMedicinale());
                        medico = daoMedici.getMedicoByID(r.getMedico());
                        out.println("<tr>" +
                                "<td>" + r.getID() + "</td>" +
                                "<td>" + medicinale.getNome() + "</td>" +
                                "<td>" + medico.getCognome() + " " + medico.getNome() + "</td>" +
                                "<td><a href=\"" + serverPath + "/DownloadRicettaServlet?rid=" + r.getID() + "\" class=\"btn btn-primary\">" +
                                "<i class=\"fa fa-download\"></i> Scarica PDF</a></td>" +
                                "</tr");
                    }
                %>
                </tbody>
            </table>

            <br/>
            <br/>

            <h3>Storico ricette ritirate</h3>
            <table class="table table-striped my-datatable" style="text-align: center;">
                <thead>
                <tr>
                    <th class="th-sm">ID Ricetta</th>
                    <th class="th-sm">Medicinale</th>
                    <th class="th-sm">Medico prescrivente</th>
                </tr>
                </thead>
                <tbody>
                <%
                    ricette = daoRicette.getRicetteByIDPaziente(IDpaziente, true);
                    for (Ricetta r : ricette) {
                        medicinale = daoMedicinali.getMedicinaleByID(r.getMedicinale());
                        medico = daoMedici.getMedicoByID(r.getMedico());
                        out.println("<tr>" +
                                "<td>" + r.getID() + "</td>" +
                                "<td>" + medicinale.getNome() + "</td>" +
                                "<td>" + medico.getCognome() + " " + medico.getNome() + "</td>" +
                                "</tr");
                    }
                %>
                </tbody>
            </table>
        </div>

        <div id="ticket" class="container tab-pane fade">

            <!-- <h3>Ricette da ritirare</h3> -->
            <table class="table table-striped my-datatable" style="text-align: center;">
                <thead>
                <tr>
                    <th class="th-sm">ID Ticket</th>
                    <th class="th-sm">Causale</th>
                    <th class="th-sm">Data</th>
                    <th class="th-sm">Importo</th>
                </tr>
                </thead>
                <tbody>
                <%
                    tickets = daoTickets.getTicketByIDPaziente(IDpaziente);
                    for (Ticket t : tickets) {
                        out.println("<tr>" +
                                "<td>" + t.getID() + "</td>" +
                                "<td>" + t.getCausale() + "</td>" +
                                "<td>" + t.getData() + "</td>" +
                                "<td>" + t.getImporto() + "</td>" +
                                "</tr");
                    }
                %>
                </tbody>
            </table>

            <br/>
            <a href="<%= serverPath %>/DownloadTicketsServlet" class="btn btn-primary float-sm-right"><i
                    class="fa fa-download"></i> Scarica PDF</a>
        </div>

        <div id="cambia-medico" class="container tab-pane fade"><br/>
            <h3>Medico di base attuale:</h3>
            <br/>
            <%
                medico = daoMedici.getMedicoByID(idMedico);
                out.println(medico.getCognome() + " " + medico.getNome());
            %>

            <br/>
            <br/>

            <h3>Cerca altro medico nella tua provincia:</h3>
            <br/>
            <form class="was-validated" method="POST" action="<%= serverPath %>/CambioMedicoServlet"
                  role="form">
                <div class="form-group">
                    <select id="nuovo-medico" name="nuovo-medico" required>
                        <option></option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Cambia dottore</button>
            </form>
        </div>

        <div id="foto" class="container tab-pane fade"><br/>
            <div class="row">
                <div class="col-lg-2">
                    <img src="data:image/jpeg;base64,<%= profilo.getImgBase64Encoded() %>" class="img-thumbnail"
                         alt="Foto profilo" height="256" width="256">
                </div>

                <div class="col-lg-2"></div>

                <div class="col-lg-8">
                    <form class="form" method="post" action="<%= serverPath %>/UploadFotoServlet"
                          enctype="multipart/form-data">
                        <div class="input-group">
                            <div class="custom-file">
                                <label class="custom-file-label" for="customFile">Seleziona immagine da caricare</label>
                                <input type="file" class="custom-file-input" id="customFile" name="upload-immagine">
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-outline-primary" type="submit" id="submit-immagine"
                                        disabled>Carica foto
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <br>
            <br>

            <div class="container">
                <div class="row">
                    <h3>Album paziente</h3>
                </div>

                <br>

                <div class="row">
                    <%
                        for (Foto f : foto) {
                            out.println(
                                    "<div class='card mb-4' style='margin: 2px;'>" +
                                            "<img class='card-img-top rounded' src='data:image/jpeg;base64," + f.getImgBase64Encoded() +
                                            "' style='height: 128px; width: 100%; display: block;'>" +
                                            "<div class='card-body'>" +
                                            "<h5 class='card-title'>" + sdf.format(f.getData()) + "</h5>" +
                                            "</div>" +
                                            "</div>");
                        }
                    %>
                </div>

            </div>
        </div>

        <div id="cambia-pw" class="container tab-pane fase"><br/>
            <div class="row">
                <div class="col">
                    <form class="form" method="post" action="<%= serverPath %>/CambiaPasswordServlet">
                        <div class="form-group">
                            <label for="new-pw">Nuova password:</label>
                            <input type="password" class="form-control" id="new-pw" placeholder="Inserire nuova password"
                                   name="new-pw" required>
                        </div>
                        <div class="form-group">
                            <label for="rep-new-pw">Ripeti password:</label>
                            <input type="password" class="form-control" id="rep-new-pw" placeholder="Ripetere nuova password"
                                   name="rep-new-pw" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Cambia password</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>
