<%--
  Created by IntelliJ IDEA.
  Author: Paolo Malacarne
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    String serverPath = request.getRequestURL().toString().replaceFirst(request.getRequestURI(), "") + request.getContextPath();
%>
<html>
<head>
    <title>Sistema Sanitario - Medico specialista</title>
    <link rel="icon" type="image/ico" href="imgs/stellone.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTable CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          integrity="sha256-nbyata2PJRjImhByQzik2ot6gSHSU4Cqdz5bNYL2zcU=" crossorigin="anonymous"/>

    <!-- Javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- DataTable js -->
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script>
        let idPaziente;
        let idEsame;

        $(document).on('hidden.bs.modal', function (event) {
            if ($('.modal:visible').length) {
                $('body').addClass('modal-open');
            }
        });

        $(document).ready(function () {

            let myTable = $("#my-datatable").DataTable({
                "scrollY": "100px",
                "scrollCollapse": false,
                "paging": false,
                "info": false,
                "language": {
                    "search": "Cerca:",
                    "emptyTable": "Non ci sono dati da visualizzare.",
                    "zeroRecords": "Nessun risultato trovato"
                }
            });
            let storicoEsami = $("#storico-esami").DataTable({
                "scrollY": "100px",
                "scrollCollapse": false,
                "paging": false,
                "info": false,
                "language": {
                    "search": "Cerca:",
                    "emptyTable": "Non ci sono dati da visualizzare.",
                    "zeroRecords": "Nessun risultato trovato"
                }
            });
            $('.myTables_length').addClass('bs-select');

            let urlLE = '<%= serverPath %>/models/lista-esami';
            $.getJSON(urlLE, function (data) {
                $('#esami').select2({
                    allowClear: true,
                    data: data,
                    placeholder: "Seleziona esami",
                    theme: "bootstrap"
                });
            });

            let urlMedicinali = '<%= serverPath %>/models/medicinali';
            $.getJSON(urlMedicinali, function (data) {
                $('#medicinali').select2({
                    allowClear: true,
                    data: data,
                    placeholder: "Seleziona medicinali",
                    theme: "bootstrap"
                });
            });

            let urlEsamiPaziente = '<%= serverPath %>/models/esami/paziente/';
            $.getJSON('<%= serverPath %>/models/pazienti', function (data) {
                $('#pazienti-suggestion').select2({
                    allowClear: true,
                    data: data,
                    placeholder: "Seleziona paziente",
                    theme: "bootstrap"
                }).on('select2:open', function () {

                    $('.select2-dropdown--above').attr('id', 'fix');
                    $('#fix').removeClass('select2-dropdown--above');
                    $('#fix').addClass('select2-dropdown--below');

                }).on('select2:select', function (e) {
                    let cognomeNomeCod = e.params.data.text;
                    if (cognomeNomeCod !== '') {
                        idPaziente = e.params.data.id;
                        $.getJSON(urlEsamiPaziente + idPaziente, function (data) {

                            storicoEsami.rows().remove().draw(false);
                            myTable.rows().remove().draw(false);
                            data.forEach(function (entry) {
                                if (entry['svolto']) {
                                    storicoEsami.row.add([
                                        entry['esame'],
                                        entry['esito'],
                                        entry['data']
                                    ]).draw(false);
                                } else {
                                    myTable.row.add([
                                        entry['esame'],
                                        '<button class="btn btn-info" onclick="erogaEsame(' + entry['id'] + ')">Eroga</button>'
                                    ]).draw(false);
                                }
                            });

                            $.getJSON('<%= serverPath %>/models/pazienti/' + idPaziente, function (data) {
                                $('#nome').text(data['nome']);
                                $('#cognome').text(data['cognome']);
                                $('#codice-fiscale').text(data['codice-fiscale']);
                                $('#data-nascita').text(data['data-nascita']);
                                $('#luogo-nascita').text(data['luogo-nascita']);
                                $('#foto').attr('src', 'data:image/jpeg;base64,' + data['foto']);

                                $("#mymodal").modal('show');
                                $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
                            }).fail(function (err) {
                                console.log(err.status);
                            });
                        }).fail(function (err) {
                            console.log(err);
                        });
                    }
                });
            });

            $('#modal-eroga-esame').submit(function (event) {
                let servletURL = "<%= serverPath %>/ErogaEsameServlet";
                let params = {
                    'id': idEsame,
                    'esito': $('#esito').val()
                };

                $.post(servletURL, params, function (response) {
                    $('#modal-eroga-esame').modal('hide');

                    $.getJSON(urlEsamiPaziente + idPaziente, function (data) {
                        storicoEsami.rows().remove().draw(false);
                        myTable.rows().remove().draw(false);
                        data.forEach(function (entry) {
                            if (entry['svolto']) {
                                storicoEsami.row.add([
                                    entry['esame'],
                                    entry['esito'],
                                    entry['data']
                                ]).draw(false);
                            } else {
                                myTable.row.add([
                                    entry['esame'],
                                    '<button class="btn btn-info" onclick="erogaEsame(' + entry['id'] + ')">Eroga</button>'
                                ]).draw(false);
                            }
                        });
                        $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
                    }).fail(function (err) {
                        console.log(err.status);
                    });
                });

                event.preventDefault();
            });

        });

        function terminaVisita() {
            let meds = [];
            let exs = [];

            $.each($('#medicinali').select2('data'), function (k, v) {
                meds.push(v['text']);
            });
            $.each($('#esami').select2('data'), function (k, v) {
                exs.push(v['text']);
            });

            let params = {
                'paziente': paziente,
                'medicinali': meds.toString(),
                'esami': exs.toString()
            };

            $.post('<%= serverPath %>/TerminaVisitaServlet', params)
                .done(function (response) {
                    location.reload();
                })
                .fail(function (err) {
                    console.log(err);
                });
        }

        function erogaEsame(id) {
            idEsame = id;
            $('#esito').val('');
            $("#modal-eroga-esame").modal('show');
        }

    </script>
</head>
<body class="text-center">

<!-- HEADER -->
<%@include file="header-logged.jsp" %>

<div class="container">


    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#visite">Visite</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#cambia-pw">Cambia password</a>
        </li>
    </ul>

    <br/>
    <br/>

    <!-- Tab panes -->
    <div class="tab-content">

        <div id="visite" class="container tab-pane active">
            <div class="container">

                <div class="row">

                    <div class="container">
                        <label for="pazienti-suggestion">Cerca paziente:</label>
                        <select id="pazienti-suggestion" class="form-control">
                            <option></option>
                        </select>
                    </div>

                    <div class="modal" id="mymodal">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Visita paziente</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">
                                    <div class="container-fluid">

                                        <div class="row">
                                            <div class="col">
                                                <img src="" class="img-thumbnail"
                                                     alt="Foto profilo" id="foto" height="192" width="192">
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="container">

                                                <h4>Scheda anagrafica</h4>

                                                <div class="row">
                                                    <div class="col-sm-4">Nome: <p id="nome"></p></div>
                                                    <div class="col-sm-4">Cognome: <p id="cognome"></p></div>
                                                    <div class="col-sm-4">Cod. Fiscale: <p id="codice-fiscale"></p></div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-sm-4">Data di nascita: <p id="data-nascita"></p></div>
                                                    <div class="col-sm-4">Luogo di nascita: <p id="luogo-nascita"></p></div>
                                                    <div class="col-sm-4"></div>
                                                </div>

                                                <br>

                                                <div class="row">
                                                    <h4>Storico</h4>
                                                    <div class="table-responsive">
                                                        <table class="table table-striped responsive" id="storico-esami"
                                                               style="text-align: center;">
                                                            <thead>
                                                            <tr>
                                                                <th class="th-sm">Esame</th>
                                                                <th class="th-sm">Esito</th>
                                                                <th class="th-sm">Data</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <br>
                                        <br>

                                        <div class="row">

                                            <div class="table-responsive">
                                                <table class="table table-striped responsive" id="my-datatable"
                                                       style="text-align: center;">
                                                    <thead>
                                                    <tr>
                                                        <th class="th-sm">Esame</th>
                                                        <th class="th-sm"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="table-body">
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="container">

                                                <div class="row">
                                                    <div class="col">
                                                        <label for="medicinali">Prescrivi medicinali:</label>
                                                        <select id="medicinali" class="form-control" multiple="multiple">
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col">
                                                        <label for="esami">Precrivi esami:</label>
                                                        <select id="esami" class="form-control" multiple="multiple">
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <button class="btn btn-primary" onclick="terminaVisita()">Termina visita</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="modal" id="modal-eroga-esame">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Esame</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">

                                    <form id="modal-form">

                                        <div class="form-group">
                                            <label for="esito">Referto esame:</label>
                                            <textarea id="esito" name="esito" class="form-control" rows="4"></textarea>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Eroga esame</button>

                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <div id="cambia-pw" class="container tab-pane fase"><br/>
            <div class="row">
                <div class="col">
                    <form class="form" method="post" action="<%= serverPath %>/CambiaPasswordServlet">
                        <div class="form-group">
                            <label for="new-pw">Nuova password:</label>
                            <input type="password" class="form-control" id="new-pw" placeholder="Inserire nuova password"
                                   name="new-pw" required>
                        </div>
                        <div class="form-group">
                            <label for="rep-new-pw">Ripeti password:</label>
                            <input type="password" class="form-control" id="rep-new-pw" placeholder="Ripetere nuova password"
                                   name="rep-new-pw" required>
                        </div>

                        <button type="submit" class="btn btn-primary">Cambia password</button>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>

</body>
</html>
