<nav class="navbar navbar-expand-lg navbar-light bg-primary">
    <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="<%=request.getContextPath()%>"><img src="imgs/stellone.png" width="32px" height="32px"> Home</a>
            </li>
        </ul>
        <form class="form-inline my-2 my-lg-0" method="POST" action="<%=request.getContextPath()%>/LogoutServlet">
            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">Logout</button>
        </form>
    </div>
</nav>

<br>
<br>