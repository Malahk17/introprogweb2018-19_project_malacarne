<%--
  Created by IntelliJ IDEA.
  Author: Paolo Malacarne
--%>
<%@ page session="true"
         contentType="text/html;charset=UTF-8"
         language="java" %>

<%@ page import="beans.Esame" %>
<%@ page import="beans.EsamePrescrivibile" %>
<%@ page import="beans.Paziente" %>
<%@ page import="dao.DAOEsami" %>
<%@ page import="dao.DAOListaEsami" %>
<%@ page import="dao.DAOPazienti" %>
<%@ page import="java.util.ArrayList" %>

<%
    String serverPath = request.getRequestURL().toString().replaceFirst(request.getRequestURI(), "") + request.getContextPath();

    DAOListaEsami daoListaEsami = new DAOListaEsami();
    DAOEsami daoEsami = new DAOEsami();
    DAOPazienti daoPazienti = new DAOPazienti();

    String provincia = session.getAttribute("ID").toString();

    ArrayList<Esame> esami = daoEsami.getEsamiBySvoltoProvincia(false, provincia);
    EsamePrescrivibile ep;
    Paziente p;
%>
<html>
<head>
    <title>Sistema Sanitario - S.S.P.</title>
    <link rel="icon" type="image/ico" href="imgs/stellone.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTable CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          integrity="sha256-nbyata2PJRjImhByQzik2ot6gSHSU4Cqdz5bNYL2zcU=" crossorigin="anonymous"/>
    <!-- JQuery UI -->
    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- Javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- DataTable js -->
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
    <!-- JQuery UI -->
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script>
        let id;

        $(document).ready(function () {
            $("#datepicker").datepicker({
                dateFormat: 'dd/mm/yy',
                maxDate: 0
            }).datepicker("setDate", new Date());

            $(".my-datatable").DataTable({
                "scrollY": "250px",
                "scrollCollapse": false,
                "paging": false,
                "info": false,
                "language": {
                    "search": "Cerca:",
                    "emptyTable": "Non ci sono dati da visualizzare.",
                    "zeroRecords": "Nessun risultato trovato"
                }
            });
            $('.dataTables_length').addClass('bs-select');

            $('#mymodal').on('shown.bs.modal', function (event) {
                $('#id-paziente').val(id);
            });
            $('#modal-form').submit(function (event) {
                let servletURL = "<%= serverPath %>/ErogaEsameServlet";
                let params = {
                    'id': id,
                    'esito': $('#esito').val()
                };

                $.post(servletURL, params, function (response) {
                    location.reload();
                });

                event.preventDefault();
            });

            let url = '<%= serverPath %>/models/lista-esami';
            $.getJSON(url, function (data) {
                $('#richiamo-suggestion').select2({
                    allowClear: true,
                    data: data,
                    placeholder: "Seleziona esame",
                    theme: "bootstrap"
                });
            });
        });

        function setIdEsame(idEsame) {
            id = idEsame;
            $('#esito').val('');
            $("#mymodal").modal('show');
        }

    </script>
</head>
<body class="text-center">

<!-- HEADER -->
<%@include file="header-logged.jsp" %>

<div class="container">

    <div class="row">

        <div class="container">

            <div class="row">

                <div class="col">
                    <h3>Report spese</h3>
                </div>

            </div>

            <br>

            <div class="row">
                <div class="col">

                    <form class="form-inline" method="get"
                          action="<%= request.getContextPath() %>/DownloadReportSSPServlet">

                        <div class="form-group">
                            <label for="datepicker" class="mr-sm-2">Report ricette erogate in data:</label>
                            <input type="text" id="datepicker" name="data-report" class="form-control mr-sm-4" required
                                   readonly>
                        </div>

                        <button type="submit" class="btn btn-primary">Scarica XLS</button>

                    </form>

                </div>
            </div>

        </div>

    </div>

    <br>
    <br>
    <br>

    <div class="row">

        <div class="container">

            <div class="row">

                <div class="col">
                    <h3>Lista esami da erogare</h3>
                </div>

            </div>

            <br>

            <div class="row">
                <div class="col">
                    <table class="table table-striped my-datatable" style="text-align: center;">
                        <thead>
                        <tr>
                            <th class="th-sm">Esame</th>
                            <th class="th-sm">Nome</th>
                            <th class="th-sm">Cognome</th>
                            <th class="th-sm">Codice Fiscale</th>
                            <th class="th-sm"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <%
                            for (Esame e : esami) {
                                ep = daoListaEsami.getEsamePrescrivibiliByID(e.getEsamePrescrivibile());
                                p = daoPazienti.getPazienteByID(e.getPaziente());
                                out.println("<tr>" +
                                        "<td>" + ep.getNome() + "</td>" +
                                        "<td>" + p.getNome() + "</td>" +
                                        "<td>" + p.getCognome() + "</td>" +
                                        "<td>" + p.getCodiceFiscale() + "</td>" +
                                        "<td><button class='btn btn-info' onclick='setIdEsame(" + e.getID() + ")'>Eroga esame</a></td>" +
                                        "</tr");
                            }
                        /*
                        "<td><a href='" + request.getContextPath() + "/ErogaEsameServlet?e=" + e.getID() + "' class='btn btn-primary'>" +
                                    "<i class='fa fa-download'></i> Scarica PDF</a></td>" +
                         */
                        %>
                        </tbody>
                    </table>

                    <div class="modal" id="mymodal">
                        <div class="modal-dialog">
                            <div class="modal-content">

                                <!-- Modal Header -->
                                <div class="modal-header">
                                    <h4 class="modal-title">Esame</h4>
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                </div>

                                <!-- Modal body -->
                                <div class="modal-body">

                                    <form id="modal-form">

                                        <div class="form-group">
                                            <label for="esito">Referto esame:</label>
                                            <textarea id="esito" name="esito" class="form-control" rows="4"></textarea>
                                        </div>

                                        <button type="submit" class="btn btn-primary">Eroga esame</button>

                                    </form>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <br>
    <br>
    <br>

    <div class="row">

        <div class="container">

            <div class="row">
                <div class="col">
                    <h3>Richiami</h3>
                </div>
            </div>

            <br>

            <div class="row">

                <div class="col">

                    <form class="form" method="post" action="<%= request.getContextPath() %>/EmettiRichiamoServlet">

                        <div class="form-group">
                            <label for="richiamo-suggestion" class="mr-sm-1">Richiamo per esame:</label>
                            <select id="richiamo-suggestion" name="richiamo-suggestion" class="form-control mr-sm-3"
                                    required>
                                <option></option>
                            </select>
                        </div>

                        <div class="form-row">

                            <div class="col">
                                <div class="form-group">
                                    <label for="min" class="mr-sm-1"> da anni </label>
                                    <input type="number" class="form-control mr-sm-3" id="min" name="min"
                                           maxlength="3" min="0" max="150">
                                </div>
                            </div>

                            <div class="col">
                                <div class="form-group">
                                    <label for="max" class="mr-sm-1"> a </label>
                                    <input type="number" class="form-control mr-sm-3" id="max" name="max"
                                           maxlength="3" min="0" max="150">
                                </div>
                            </div>

                        </div>

                        <button type="submit" class="btn btn-primary">Emetti richiamo</button>

                    </form>

                </div>

            </div>

        </div>

    </div>

</div>

</body>
</html>
