<%--
  Created by IntelliJ IDEA.
  Author: Paolo Malacarne
--%>
<%@ page session="true"
         contentType="text/html;charset=UTF-8"
         language="java" %>

<%
    String serverPath = request.getRequestURL().toString().replaceFirst(request.getRequestURI(), "") + request.getContextPath();
%>
<html>
<head>
    <title>Sistema Sanitario - Farmacia</title>
    <link rel="icon" type="image/ico" href="imgs/stellone.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- DataTable CSS -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          integrity="sha256-nbyata2PJRjImhByQzik2ot6gSHSU4Cqdz5bNYL2zcU=" crossorigin="anonymous"/>

    <!-- Javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- DataTable js -->
    <script type="text/javascript" charset="utf8"
            src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script>
        $(document).ready(function () {
            let url1 = '<%= serverPath %>/PazientiServlet';
            let url2 = '<%= serverPath %>/RicettePazienteServlet';
            let pazientiSuggestion = $('#pazienti-suggestion');
            let myModal = $("#mymodal");

            $.getJSON(url1, function (data) {
                pazientiSuggestion.select2({
                    allowClear: true,
                    data: data,
                    placeholder: "Seleziona paziente",
                    theme: "bootstrap"
                }).on('select2:open', function () {

                    $('.select2-dropdown--above').attr('id', 'fix');
                    $('#fix').removeClass('select2-dropdown--above');
                    $('#fix').addClass('select2-dropdown--below');

                });
            });

            let myTable = $("#my-datatable").DataTable({
                "scrollY": "250px",
                "scrollCollapse": false,
                "paging": false,
                "info": false,
                "language": {
                    "search": "Cerca:",
                    "emptyTable": "Non ci sono dati da visualizzare.",
                    "zeroRecords": "Nessun risultato trovato"
                }
            });
            $('.myTables_length').addClass('bs-select');

            pazientiSuggestion.on('select2:select', function (e) {
                var cognomeNomeCod = e.params.data.text;
                if (cognomeNomeCod !== '') {
                    $.getJSON(url2, {paziente: cognomeNomeCod})
                        .done(function (data) {
                            myTable.rows().remove().draw();
                            data.forEach(function (entry) {
                                myTable.row.add([
                                    entry['id'],
                                    entry['medicinale'],
                                    '<button class="btn btn-info" onclick="erogaRicetta(' + entry['id'] + ')">Eroga</button>'
                                ]).draw(false);
                            });
                            myModal.modal('show');
                            $.fn.dataTable.tables({visible: true, api: true}).columns.adjust();
                        })
                        .fail(function (err) {
                            console.log(err);
                        });
                }
            });

        });

        function erogaRicetta(id) {
            $.post("<%= serverPath %>/ErogaRicettaServlet", {'id-ricetta': id})
                .done(function (response) {
                    location.reload();
                })
                .fail(function (err) {
                    console.log(err);
                });
        }
    </script>
</head>
<body class="text-center">

<!-- HEADER -->
<%@include file="header-logged.jsp" %>

<div class="container">

    <div class="row">

        <div class="container">
            <label for="pazienti-suggestion">Cerca paziente:</label>
            <select id="pazienti-suggestion" class="form-control">
                <option></option>
            </select>
        </div>

    </div>

    <br>
    <br>

    <div class="row">

        <div class="modal" id="mymodal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">Paziente</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">

                        <table class="table table-striped" id="my-datatable" style="text-align: center;">
                            <thead>
                            <tr>
                                <th class="th-sm">ID Ricetta</th>
                                <th class="th-sm">Medicinale</th>
                                <th class="th-sm"></th>
                            </tr>
                            </thead>
                            <tbody id="table-body">
                            </tbody>
                        </table>

                    </div>

                </div>
            </div>
        </div>

    </div>

</div>

</body>
</html>
