<%--
  Created by IntelliJ IDEA.
  Author: Paolo Malacarne
--%>
<%@ page session="true"
         contentType="text/html;charset=UTF-8"
         language="java" %>

<%
    String serverPath = request.getRequestURL().toString().replaceFirst(request.getRequestURI(), "") + request.getContextPath();
%>
<html>
<head>
    <title>Sistema Sanitario - S.S.N.</title>
    <link rel="icon" type="image/ico" href="imgs/stellone.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Font Awesome CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/select2-bootstrap-theme/0.1.0-beta.10/select2-bootstrap.min.css"
          integrity="sha256-nbyata2PJRjImhByQzik2ot6gSHSU4Cqdz5bNYL2zcU=" crossorigin="anonymous"/>

    <!-- Javascript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    <!-- Select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>

    <script>

        $(document).ready(function () {

            let url1 = '<%= serverPath %>/ProvinceServlet';
            let url2 = '<%= serverPath %>/MediciProvinciaServlet';
            let selectProvincia = $('#select-provincia');
            let selectMedico = $('#select-medico');

            $.getJSON(url1, function (data) {
                selectProvincia.select2({
                    allowClear: true,
                    data: data,
                    placeholder: "Seleziona provincia",
                    theme: "bootstrap"
                });
            });

            selectProvincia.on('select2:select', function (e) {
                var data = e.params.data;
                if (data !== '') {
                    $.getJSON(url2, {provincia: data.text}, function (data) {
                        if (selectMedico.hasClass('select2-hidden-accessible')) {
                            selectMedico.html('').select2({data: [{id: '', text: ''}]});
                        }
                        selectMedico.select2({
                            allowClear: true,
                            data: data,
                            placeholder: "Seleziona medico",
                            theme: "bootstrap"
                        });
                        selectMedico.prop('disabled', false);
                    });
                }
            });

            selectProvincia.on('select2:clear', function (e) {
                selectMedico.prop('disabled', true);
            });

        });

    </script>
</head>
<body class="text-center">

<!-- HEADER -->
<%@include file="header-logged.jsp" %>

<div class="container">

    <div class="row">

        <div class="col">

            <form class="form" method="get" action="<%= request.getContextPath() %>/DownloadReportSSNServlet">

                <div class="form-group">
                    <label for="select-provincia" class="mr-sm-1">Provincia:</label>
                    <select id="select-provincia" name="select-provincia" class="form-control mr-sm-3" required>
                        <option></option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="select-medico" class="mr-sm-1">Medico:</label>
                    <select id="select-medico" name="select-medico" class="form-control mr-sm-3" disabled>
                        <option></option>
                    </select>
                </div>

                <button type="submit" id="submit" class="btn btn-primary">Download Report</button>

            </form>

        </div>

    </div>

</div>

</body>
</html>
