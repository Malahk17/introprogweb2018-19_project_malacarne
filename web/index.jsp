<%--
  Created by IntelliJ IDEA.
  Author: Paolo Malacarne
--%>
<%@ page contentType="text/html;charset=UTF-8"
         language="java" %>

<%
    String serverPath = request.getRequestURL().toString().replaceFirst(request.getRequestURI(), "") + request.getContextPath();
%>
<html>
<head>
    <title>Sistema Sanitario Nazionale</title>
    <link rel="icon" type="image/ico" href="imgs/stellone.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body class="text-center">

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#"><img src="imgs/stellone.png"/> Servizio Sanitario Nazionale</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="btn btn-primary" href="login.jsp" role="button">Login</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Header -->
<header class="bg-primary py-5 mb-5">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-12">
                <br><br><br>
                <h1 class="display-4 text-white mt-5 mb-2">Servizio Sanitario Nazionale</h1>
                <p class="lead mb-5 text-white-50">
                    La salute è diritto di tutti, tutelato dalla Costituzione italiana
                </p>
            </div>
        </div>
    </div>
</header>

<!-- Page Content -->
<div class="container">

    <div class="row">
        <div class="col-md-8 mb-5">
            <h2>Chi siamo</h2>
            <hr>
            <p>
                Il Servizio sanitario nazionale (SSN) è un sistema di strutture e servizi che hanno lo scopo di
                garantire a tutti i cittadini, in condizioni di uguaglianza, l’accesso universale all’erogazione equa
                delle prestazioni sanitarie, in attuazione dell’art.32 della Costituzione, che recita:
            </p>
            <p>
                "La Repubblica tutela la salute come fondamentale diritto dell'individuo e interesse della collettività,
                e garantisce cure gratuite agli indigenti. Nessuno può essere obbligato a un determinato trattamento
                sanitario se non per disposizione di legge. La legge non può in nessun caso violare i limiti imposti dal
                rispetto della persona umana".
            </p>
            <a class="btn btn-primary btn-lg" href="http://www.salute.gov.it/portale/home.html">Scopri di più &raquo;</a>
        </div>
        <div class="col-md-4 mb-5">
            <h2>Contatti</h2>
            <hr>
            <address>
                <strong>Ministero della Salute</strong>
                <br><a href="http://www.salute.gov.it/portale/ministro/p4_7.jsp?lingua=italiano&label=urp">
                Ufficio relazioni con il pubblico - Urp</a>
                <br>email: <a href="http://www.salute.gov.it/portale/p_sendMail2.jsp">Scrivi all'Urp</a>
                <br>
            </address>
            <address>
                <a href="http://www.salute.gov.it/portale/ministro/p4_5_2_4_1.jsp?lingua=italiano&menu=uffCentrali&label=uffCentrali&id=1151">Direzione generale della programmazione sanitaria</a>
                <br>Viale Giorgio Ribotta, 5
                <br>00144 - Roma
            </address>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6 mb-5">
            <h2>I princìpi del Servizio sanitario nazionale (SSN)</h2>
            <hr>
            <p>
                I principi fondamentali su cui si basa il SSN dalla sua istituzione, avvenuta con la legge n.833 del 1978, sono l’universalità, l’uguaglianza e l’equità.
            </p>
            <p>
                Universalità significa l’estensione delle prestazioni sanitarie a tutta la popolazione. In osservanza del nuovo concetto di salute introdotto dalla legge di istituzione del SSN. La salute, a partire dal 1978, è stata intesa infatti non soltanto come bene individuale ma soprattutto come risorsa della comunità.
            </p>
            <p>
                I cittadini devono accedere alle prestazioni del SSN senza nessuna distinzione di condizioni individuali, sociali ed economiche. Ai cittadini, che non appartengono a categorie esenti, è richiesto il pagamento di un ticket che varia per ogni singola prestazione prevista dai LEA.
            </p>
            <p>
                A tutti i cittadini deve essere garantita parità di accesso in rapporto a uguali bisogni di salute. Questo è il principio fondamentale che ha il fine di superare le diseguaglianze di accesso dei cittadini alle prestazioni sanitarie.
            </p>
        </div>
        <div class="col-md-6 mb-5">
            <h2>Cosa sono i LEA</h2>
            <hr>
            <p>
                I Livelli essenziali di assistenza (LEA) sono le prestazioni e i servizi che il Servizio sanitario nazionale (SSN) è tenuto a fornire a tutti i cittadini, gratuitamente o dietro pagamento di una quota di partecipazione (ticket), con le risorse pubbliche raccolte attraverso la fiscalità generale (tasse).
            </p>
            <p>
                Per garantire l’aggiornamento continuo, sistematico, su regole chiare e criteri scientificamente validi dei Livelli essenziali di assistenza, è stata istituita la Commissione nazionale per l'aggiornamento dei LEA e la promozione dell’appropriatezza nel Servizio sanitario nazionale.
            </p>
            <h2>La prevenzione nei LEA</h2>
            <hr>
            <p>
                l livello della “Prevenzione collettiva e sanità pubblica” include le attività e le prestazioni volte a tutelare la salute e la sicurezza della comunità da rischi infettivi, ambientali, legati alle condizioni di lavoro, correlati agli stili di vita.
            </p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 mb-5">
            <div class="card h-100">
                <img class="card-img-top" src="imgs/cbfi.jpg" alt="">
                <div class="card-body">
                    <h4 class="card-title">"Il cancro si può battere. Facciamolo insieme"e</h4>
                    <p class="card-text">Ministero della Salute e IFO per la Giornata mondiale contro il cancro </p>
                </div>
                <div class="card-footer">
                    <a href="http://www.salute.gov.it/portale/lea/dettaglioNotizieLea.jsp?lingua=italiano&id=4041" class="btn btn-primary">Vai all'articolo</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-5">
            <div class="card h-100">
                <img class="card-img-top" src="imgs/LEA.jpg" alt="">
                <div class="card-body">
                    <h4 class="card-title">Richiesta di aggiornamento dei LEA</h4>
                    <p class="card-text">Le richieste di inclusione, esclusione o aggiornamento delle prestazioni e dei servizi inclusi nei LEA possono essere avanzate, tramite posta elettronica certificata, direttamente dal Ministero della Salute o dalle Istituzioni da esso vigilate (AIFA, AGENAS, ISS) e dalle Regioni e/o Province Autonome.</p>
                </div>
                <div class="card-footer">
                    <a href="http://www.salute.gov.it/portale/lea/dettaglioContenutiLea.jsp?lingua=italiano&id=5158&area=Lea&menu=aggLea" class="btn btn-primary">Vai all'articolo</a>
                </div>
            </div>
        </div>
        <div class="col-md-4 mb-5">
            <div class="card h-100">
                <img class="card-img-top" src="imgs/40anni.jpg" alt="">
                <div class="card-body">
                    <h4 class="card-title">Celebrazione dei 40 anni del SSN</h4>
                    <p class="card-text">E’ l’evento del ministero della Salute per celebrare il quarantesimo anniversario della nascita del SSN, alla presenza del Presidente della Repubblica, Sergio Mattarella, e del ministro della Salute, Giulia Grillo, con le testimonianze dei protagonisti del sistema sanitario.</p>
                </div>
                <div class="card-footer">
                    <a href="http://www.salute.gov.it/portale/lea/dettaglioVideoLea.jsp?lingua=italiano&menu=multimedia&p=video&id=1880" class="btn btn-primary">Vai all'articolo</a>
                </div>
            </div>
        </div>
    </div>

</div>

<!-- Footer -->
<footer class="py-5 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Template pagina: <a href="https://startbootstrap.com/templates/business-frontpage/">Start Bootstrap - Business frontpage</a></p>
        <p class="m-0 text-center text-white">Licenza template MIT</p>
    </div>
</footer>

</body>
</html>
