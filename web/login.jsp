<%--
  Created by IntelliJ IDEA.
  Author: Paolo Malacarne
--%>
<%@ page session="true"
         contentType="text/html;charset=UTF-8"
         language="java" %>

<%
    String serverPath = request.getRequestURL().toString().replaceFirst(request.getRequestURI(), "") + request.getContextPath();
%>
<html>
<head>
    <title>Login</title>
    <link rel="icon" type="image/ico" href="imgs/stellone.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <!-- Javascript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body>


<br>
<br>

<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <div class="card card-signin my-5">
                <div class="card-body">
                    <h5 class="card-title text-center">Login</h5>
                    <form class="form-signin" method="POST" action="<%= serverPath %>/LoginServlet">
                        <div class="form-label-group">
                            <label for="inputEmail">Indirizzo email</label>
                            <input type="email" id="inputEmail" class="form-control" name="email"
                                   placeholder="Inserire email" required autofocus
                                   value="<%= session.getAttribute("invalid-credentials") != null ? session.getAttribute("email") : "" %>">
                            <div class="invalid-feedback">Inserire l'email.</div>
                        </div><br>

                        <div class="form-label-group">
                            <label for="inputPassword">Password</label>
                            <input type="password" id="inputPassword" name="password" class="form-control"
                                   placeholder="Inserire password" required>
                            <div class="invalid-feedback">Inserire la password.</div>
                        </div><br>

                        <div class="custom-control custom-checkbox mb-3">
                            <input type="checkbox" class="custom-control-input" id="customCheck1" name="remember">
                            <label class="custom-control-label" for="customCheck1">Ricordami</label>
                        </div><br>

                        <%
                            if (session.getAttribute("invalid-credentials") != null) {
                                out.println("<div class=\"alert alert-danger alert-dismissible fade show\">Credenziali errate!</div>");
                                session.removeAttribute("invalid-credentials");
                                session.removeAttribute("email");
                            }
                        %>

                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
